﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIEvents : MonoBehaviour {

	public GameObject frostCard;
	public GameObject abandonedWagonCard;
	public GameObject deadOfNightCard;
	public GameObject okayButton;

	private List<GameObject> cards = new List<GameObject> ();

	private GameObject activeCard;

	void Awake(){
		cards.Add (frostCard);
		cards.Add (abandonedWagonCard);
		cards.Add (deadOfNightCard);

		for (int i = 0; i < cards.Count; i++) {
			cards [i].SetActive (false);
		}
		okayButton.SetActive (false);

		OotsEvents.NewEvent.AddListener (ShowEventCard);
	}

	public void OnOkayPressed(){
		okayButton.SetActive (false);
		activeCard.SetActive (false);
		OotsEvents.EventOkayPressed.Invoke ();
	}

	private void ShowEventCard(int cardNum){
		cards [cardNum].SetActive (true);
		okayButton.SetActive (true);
		activeCard = cards [cardNum];
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
