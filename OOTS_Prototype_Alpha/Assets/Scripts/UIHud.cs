﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class UIHud : MonoBehaviour {
    GameManager GM;
    PlayerCore localPlayer;

    // UI element refs
    private GameObject inventoryPanel;

    GameObject injuredImage;
    GameObject botInjuredImage;

    private Text timeDisplayText;
    private Text characterDisplayText;

    private Text exposureDisplayText;
    private Text hungerDisplayText;
    private Text healthDisplayText;
    private Text tirednessDisplayText;

    private HUDMeter sleepMeter;
    private HUDMeter hungerMeter;
    private HUDMeter healthMeter;

    void Awake() {
        GM = FindObjectOfType<GameManager>();
        localPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCore>();

        characterDisplayText = GameObject.Find("Character Display Text").GetComponent<Text>();
        timeDisplayText = GameObject.Find("Time Display Text").GetComponent<Text>();

        exposureDisplayText = GameObject.Find("Text (Exposure)").GetComponent<Text>();
        hungerDisplayText = GameObject.Find("Text (Hunger)").GetComponent<Text>();
        tirednessDisplayText = GameObject.Find("Text (Tiredness)").GetComponent<Text>();
        healthDisplayText = GameObject.Find("Text (Health)").GetComponent<Text>();

        inventoryPanel = GameObject.Find("Inventory Panel");
        injuredImage = GameObject.Find("Image (Injury)");
        botInjuredImage = GameObject.Find("Image (Bot Injury)");

        sleepMeter = transform.FindChild("Sleep Meter").GetComponent<HUDMeter>();
        hungerMeter = transform.FindChild("Hunger Meter").GetComponent<HUDMeter>();
        healthMeter = transform.FindChild("Health Meter").GetComponent<HUDMeter>();
    }

    void OnEnable() {
        OotsEvents.ModifiedInventory.AddListener(OnLocalPlayerInventoryContentsModified);
        OotsEvents.ModifiedStatus.AddListener(OnLocalPlayerStatusModified);
        OotsEvents.PlayerSpawned.AddListener(OnPlayerSpawned);
    }

    void OnDisable() {
        OotsEvents.ModifiedInventory.RemoveListener(OnLocalPlayerInventoryContentsModified);
        OotsEvents.ModifiedStatus.RemoveListener(OnLocalPlayerStatusModified);
        OotsEvents.PlayerSpawned.RemoveListener(OnPlayerSpawned);
    }

    //void Update() {    }

    void OnPlayerSpawned() {
        characterDisplayText.text = localPlayer.characterName;
    }

    private void OnLocalPlayerInventoryContentsModified() {
        // inventoryDisplayNeedsUpdate = true; then do the update in Update()
        UpdateInventoryDisplay();
    }


    private void OnLocalPlayerStatusModified() {
        healthDisplayText.text = "Health: " + localPlayer.status.currentHealth;
        timeDisplayText.text = "Day " + GM.day + ": " + GM.phase;
        hungerDisplayText.text = "Hunger: " + localPlayer.status.hunger;
        exposureDisplayText.text = "Exposure: " + localPlayer.status.exposure;
        tirednessDisplayText.text = "Tiredness: " + localPlayer.status.tiredness;


        float unit = 100.0f / 10.0f; // TODO: set based on max hunger / max tiredness

        // hunger: 0 = full meter  10 = empty meter
        hungerMeter.SetFilledLevel(unit * (10 - localPlayer.status.hunger));

        // sleep: 0 = full meter   10 = empty meter
        sleepMeter.SetFilledLevel(unit * (10 - localPlayer.status.tiredness));

        // health: max = full meter  0 = empty meter
        unit = 100.0f / localPlayer.status.maximumHealth;
        healthMeter.SetFilledLevel(unit * localPlayer.status.currentHealth);

        if (localPlayer.status.isInjured) {
            injuredImage.SetActive(true);
        }
        else {
            injuredImage.SetActive(false);
        }
    }

    [SerializeField]
    GameObject baseItemPrefab;


    private void UpdateInventoryDisplay() {
        // Update inventory current/max display text
        Text inventoryCountText = GameObject.Find("Text (Inventory Count)").GetComponent<Text>();
        inventoryCountText.text = localPlayer.inventory.Count + "/" + PlayerCore.inventoryCapacity;

        // Remove all objects from inventory item display
        foreach (Transform item in inventoryPanel.transform) {
            Destroy(item.gameObject);
        }

        // Populate inventory panel with (clickable) item icons
        foreach (var item in localPlayer.inventory) {
            GameObject baseIcon = Instantiate(baseItemPrefab);
            GameObject iconModel = Instantiate(App.itemData[item].UIprefab);
            iconModel.transform.SetParent(baseIcon.transform, false);
            baseIcon.transform.SetParent(inventoryPanel.transform, false);

            var clickScript = baseIcon.GetComponent<OnClickTrigger>();
            clickScript.SetItemName(item);
        }
    }

    private void OnUseItemPressed(string item) {
        BoardAction ba = new BoardAction();
        ba.actionType = ActionType.UseItem;
        ba.item = item;
        OotsEvents.ChoseGameAction.Invoke(ba);
    }

}
