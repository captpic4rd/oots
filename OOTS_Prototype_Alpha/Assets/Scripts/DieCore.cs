﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DieCore : MonoBehaviour {

	public bool AttackDie;
	public delegate void DieLanded(DieCore d);
	public DieLanded dieLanded;
	public int face = -1;
	public int id = -1;

	[HideInInspector]
	private Rigidbody rb;
	private bool gotFace = false;
	private bool still = false;
	private float waitTime = 0.75f;
	private float startWait;
	private Renderer[] rends;
	private List<float> diffs;
	// Use this for initialization
	void Awake(){
		rends = GetComponentsInChildren<Renderer> ();
		rb = GetComponent<Rigidbody>();
		startWait = Time.time;
	}

	int CalculateFace(){
		//Debug.DrawRay (transform.position, new Vector3 (0, 2, 0));
		Vector3[] faceVectors = new Vector3[]{ transform.forward, -transform.forward, transform.right, -transform.right, transform.up, -transform.up};

		Vector3 upMostFaceVector = faceVectors [0];

		for (int i = 0; i < 6; i++) {
			float diff = Vector3.up.y - faceVectors [i].y;
			float lowest = Vector3.up.y - upMostFaceVector.y;
			if (diff < lowest) {
				upMostFaceVector = faceVectors [i];
			}
		}

		//Debug.DrawRay (transform.position, upMostFaceVector * 3, Color.red);

		if (AttackDie) {
			if (upMostFaceVector == transform.up) {
				return 1;
			} 

			if (upMostFaceVector == -transform.up) {
				return 0;
			}

			if (upMostFaceVector == transform.right) {
				return 0;
			}

			if (upMostFaceVector == -transform.right) {
				return 0;
			}

			if (upMostFaceVector == transform.forward) {
				return 1;
			}

			if (upMostFaceVector == -transform.forward) {
				return 1;
			} else {
				print ("Error in DieCore finding upmost face");
				//rends[0].material.SetColor ("_Color", Color.red);
				return 0;
			}
		} else {
			if (upMostFaceVector == transform.up) {
				return 6;
			} 

			if (upMostFaceVector == -transform.up) {
				return 1;
			}

			if (upMostFaceVector == transform.right) {
				return 4;
			}

			if (upMostFaceVector == -transform.right) {
				return 3;
			}

			if (upMostFaceVector == transform.forward) {
				return 5;
			}

			if (upMostFaceVector == -transform.forward) {
				return 2;
			} else {
				return 0;
			}
		}
	}

	// Update is called once per frame

	void Update () {
		//DebugLines ();

		if(VelocityZero()){
			if (Time.time - startWait >= waitTime) {
				if (!gotFace) {
					face = CalculateFace ();
					gotFace = true;
					dieLanded (this);
				}
			}
		}else{
			startWait = Time.time;
			gotFace = false;
			still = false;
		}
	}

	public void AddForce(Vector3 force){
		rb.AddForce(force);
		rb.AddTorque(force);
		rends[0].material.SetColor ("_Color", Color.white);
	}

	public void EnableGravity(){
		rb.useGravity = true;
	}

	public void DisableGravity(){
		rb.useGravity = false;
	}

	private bool VelocityZero(){

		if (rb.velocity.x > 0.0f) {
			return false;
		}

		if (rb.velocity.y > 0.0f) {
			return false;
		}

		if (rb.velocity.z > 0.0f) {
			return false;
		}

		if (rb.angularVelocity.x > 0.0f) {
			return false;
		}

		if (rb.angularVelocity.y > 0.0f) {
			return false;
		}

		if (rb.angularVelocity.z > 0.0f) {
			return false;
		}

		return true;
	}
}
