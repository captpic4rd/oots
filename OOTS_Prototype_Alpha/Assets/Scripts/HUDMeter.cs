﻿using UnityEngine;
using System.Collections;

public class HUDMeter : MonoBehaviour {
    [SerializeField]
    private int top;
    [SerializeField]
    private int bottom;

    private float unitsPerPercent;
    private RectTransform mask;

    // Set the height of the mask to be X percent of the way between bottom and top.
    // bottom and top are set in the inspector according to the image
    public void SetFilledLevel(float percent) {
        if (percent < 0) percent = 0;

        mask.sizeDelta = new Vector2(mask.sizeDelta.x, bottom + unitsPerPercent * percent);

        //mask.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 50.0f);
    }

    void Awake() {
        unitsPerPercent = (top - bottom) / 100.0f;
        mask = transform.FindChild("Mask").GetComponent<RectTransform>();
    }

}
