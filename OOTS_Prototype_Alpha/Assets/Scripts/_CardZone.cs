﻿using UnityEngine;
using System.Collections.Generic;

#pragma warning disable 0414
#pragma warning disable 0168

//
// This needs a complete rewrite
// Start disabled, enable from GameManager
//

public class _CardZone : MonoBehaviour {
    private GameManager GM;
    
    // GameMaster (Card) Zone management
    private Renderer weatherCardRenderer;

    private Renderer eventCardRenderer;
    private Renderer player1CardRenderer;
    private Renderer player2CardRenderer;
    private Renderer player3CardRenderer;

    // card textures
    public Material clearSkiesMaterial;
    public Material snowMaterial;
    public Material hailMaterial;
    public Material blizzardMaterial;

    public Material reinarMaterial;
    public Material valMaterial;
    public Material enyoMaterial;


    private Weather currentWeather = Weather.Clear;

    void Awake() {
        GM = FindObjectOfType<GameManager>();
        weatherCardRenderer = GameObject.Find("Card Holder (Weather)").GetComponentInChildren<Renderer>();
        eventCardRenderer = GameObject.Find("Card Holder (Event)").GetComponentInChildren<Renderer>();
        player1CardRenderer = GameObject.Find("Card Holder (Player1)").GetComponentInChildren<Renderer>();
        player2CardRenderer = GameObject.Find("Card Holder (Player2)").GetComponentInChildren<Renderer>();
        player3CardRenderer = GameObject.Find("Card Holder (Player3)").GetComponentInChildren<Renderer>();
    }

    void Start() {
        weatherCardRenderer.material = clearSkiesMaterial;
    }

    void Update() {
        if (currentWeather != GM.weather) {
            if (GM.weather == Weather.Clear) {
                weatherCardRenderer.material = clearSkiesMaterial;
            }
            else if (GM.weather == Weather.Snow) {
                weatherCardRenderer.material = snowMaterial;
            }
            else if (GM.weather == Weather.Hail) {
                weatherCardRenderer.material = hailMaterial;
            }
            else if (GM.weather == Weather.Blizzard) {
                weatherCardRenderer.material = blizzardMaterial;
            }
            currentWeather = GM.weather;
        }
    }


    Dictionary<string, Material> playerCardMaterialMapping;

    bool doOnce = false;
    void LateUpdate() {
        if (!doOnce) {
            doOnce = !doOnce;

            playerCardMaterialMapping = new Dictionary<string, Material>() {
                { "Val", valMaterial },
                { "Enyo", enyoMaterial },
                { "Reinar", reinarMaterial }
            };

            var p1name = GM.players[0].GetComponent<PlayerCore>().characterName;
            player1CardRenderer.material = playerCardMaterialMapping[p1name];
            //player1CardRenderer.material = GM.gameSettings.cardData[p1name].material;

            var p2name = GM.players[1].GetComponent<PlayerCore>().characterName;
            player2CardRenderer.material = playerCardMaterialMapping[p2name];

            var p3name = GM.players[2].GetComponent<PlayerCore>().characterName;
            player3CardRenderer.material = playerCardMaterialMapping[p3name];

        }
    }
}

#pragma warning restore 0414
#pragma warning restore 0168