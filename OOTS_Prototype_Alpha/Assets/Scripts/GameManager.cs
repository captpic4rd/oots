﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;

public enum Phase { Morning, Noon, Afternoon, Night }
public enum Weather { Clear, Snow, Hail, Blizzard, Blizzard2, Blizzard3, Blizzard4, Count }

public class GameManager : MonoBehaviour {
    const int lastDay = 15;

    // global game state
    [HideInInspector]
    public int day = 1;
    [HideInInspector]
    public Phase phase = Phase.Morning;
    [HideInInspector]
    public Weather weather = Weather.Clear;

    // players / prefabs
    [SerializeField]
    private GameObject basePlayerPrefab;
    [SerializeField]
    private GameObject valMesh;
    [SerializeField]
    private GameObject enyoMesh;
    [SerializeField]
    private GameObject reinarMesh;


    [HideInInspector]
    public List<GameObject> players = new List<GameObject>(); // players entered into list in turn order
    [HideInInspector]
    public GameObject localPlayer;

	private bool eventOkayPressed = false;
	private int currentEvent;


    void Awake() {
        Dictionary<string, GameObject> playerPrefabMapping = new Dictionary<string, GameObject>() {
            { "Val", valMesh },
            { "Enyo", enyoMesh},
            { "Reinar", reinarMesh}
        };

        string selectedCharacter = App.instance.p1Character;

        // Setup human player
        localPlayer = GameObject.FindGameObjectWithTag("Player");
        GameObject mesh = Instantiate(playerPrefabMapping[selectedCharacter]);
        mesh.transform.SetParent(localPlayer.transform);
        localPlayer.GetComponent<PlayerCore>().InitializeHumanPlayer(selectedCharacter);
        players.Add(localPlayer);

        // Setup AI players
        List<string> temporaryPlayerList = new List<string> { "Val", "Reinar", "Enyo" };
        temporaryPlayerList.Remove(selectedCharacter);


        for (int i = 0; i < 2; i++) {
            GameObject bot = Instantiate(basePlayerPrefab);
            mesh = Instantiate(playerPrefabMapping[temporaryPlayerList[i]]);
            mesh.transform.SetParent(bot.transform);
            bot.GetComponent<PlayerCore>().InitializeBot(temporaryPlayerList[i]);
            players.Add(bot);
        }

		OotsEvents.GotWeatherRoll.AddListener (OnGotWeatherRoll);
		OotsEvents.EventOkayPressed.AddListener (OnEventOkayPressed);

        // Start main loop
        StartCoroutine("GameMain");
    }

	private bool gotNewWeather = false;
    IEnumerator GameMain() {
        yield return new WaitForSeconds(0.5f);

        while (true) {
            if (phase == Phase.Morning) {
                if (day == lastDay) {
                    // survived to day X: Set gameover status and end game
                    Debug.Log("Survived to day " + day);
                    App.SwitchScene("Main Menu");
                }
				//this event is calls the dice weather roll in DiceManager
				OotsEvents.ChangeWeather.Invoke ();
				yield return new WaitUntil (() => gotNewWeather == true);
				gotNewWeather = false;
				yield return new WaitForSeconds (.5f);
				//this function invokes the event that displays the Event card in UIEvents.cs
                DrawEventCard();
				yield return new WaitUntil (() => eventOkayPressed == true);
				eventOkayPressed = false;
            }

            // Execute turns
            for (int i = 0; i < players.Count; i++) {
                var playerCore = players[i].GetComponent<PlayerCore>();
                playerCore.StartTurn();
                yield return new WaitWhile(() => playerCore.isMyTurn);
                yield return new WaitForSeconds(0.25f);
            }

            AdvanceTimeCycle();

            yield return new WaitForSeconds(.5f);
        }
    }

    public void PlayerDeath(GameObject player) {
        players.Remove(player);
    }

    void AdvanceTimeCycle() { // TODO: sun movement and lighting. maybe skybox stuff
        if (phase == Phase.Morning) { phase = Phase.Noon; }
        else if (phase == Phase.Noon) { phase = Phase.Afternoon; }
        else if (phase == Phase.Afternoon) { phase = Phase.Night; }
        else {
            phase = Phase.Morning;
            day++;
        }
    }

    void DrawEventCard() {
		int num = Random.Range (0, 2);
		//num = 2;
		currentEvent = num;
		OotsEvents.NewEvent.Invoke (num);

		if (num == 0) {
			OotsEvents.FrostEvent.Invoke ();
		}

		if (num == 1) {
			int whichLocation = Random.Range (0, 11);
			OotsEvents.WagonEvent.Invoke (whichLocation);
		}

		if (num == 2) {
			
		}
    }

	void OnGotWeatherRoll(int result){
		weather = (Weather)result;
		Debug.Log ("Weather changed to: " + (Weather)result);
		gotNewWeather = true;
	}

	void OnEventOkayPressed(){
		if (currentEvent == 2) {
			OotsEvents.DarkNightEvent.Invoke ();
		}
		eventOkayPressed = true;
	}

    public void RandomizeWeather() {
        int rand = Random.Range(0, (int)Weather.Count);
        weather = (Weather)rand;
    }
    
}
