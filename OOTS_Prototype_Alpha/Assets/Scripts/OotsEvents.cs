﻿using UnityEngine;
using UnityEngine.Events;


// TODO: consider ditching BoardActions and just direct-calling PlayerCore methods

public enum ActionType { TakeItem, StowItem, Gather, Hunt, Study, Rest, Attack, Steal, UseItem }

public class BoardAction {
    public ActionType actionType;
    public string item;
    public Location location;
    public PlayerCore targetPlayer;
}


public class IntEvent : UnityEvent<int> { }
public class BoolEvent : UnityEvent<bool> { }
public class StringEvent : UnityEvent<string> { }

public class BoardActionEvent : UnityEvent<BoardAction> { }

public class LocationEvent : UnityEvent<Location> { }

public class PlayerEvent : UnityEvent<PlayerCore> { }



public static class OotsEvents {

    public static UnityEvent StartedTurn = new UnityEvent();
    public static UnityEvent EndedTurn = new UnityEvent();
	public static UnityEvent NewDay = new UnityEvent();

    //public static UnityEvent PerformedAction = new UnityEvent();


    public static StringEvent CombatHit = new StringEvent();

	//inform diceController that it should throw the dice
	public static IntEvent ChoseAttack = new IntEvent();

	//inform Battle Manager that the int from the dice is ready
	public static IntEvent GotAttackDiceResult = new IntEvent();

	public static UnityEvent AttemptedActionWhileInjured = new UnityEvent ();
	public static IntEvent GotInjuryDieResult = new IntEvent();

	public static IntEvent EndingTurnAtNightWithExposure = new IntEvent();
	public static BoolEvent GotExposureDieResult = new BoolEvent ();

	public static UnityEvent ChangeWeather = new UnityEvent();
	public static IntEvent GotWeatherRoll = new IntEvent();

    // inform Fog of War system that local player has changed regions
    public static IntEvent ChangedRegion = new IntEvent(); // change to LocationEvent type?

	public static IntEvent NewEvent = new IntEvent();
	public static UnityEvent EventOkayPressed = new UnityEvent();

	public static UnityEvent FrostEvent = new UnityEvent();
	public static IntEvent WagonEvent = new IntEvent();
	public static UnityEvent DarkNightEvent = new UnityEvent();
	public static UnityEvent DoDarkNightRoll = new UnityEvent ();
	public static IntEvent GotDarkNightRollResults = new IntEvent();

    // inform UI that local player has been instantiated
    public static UnityEvent PlayerSpawned = new UnityEvent();


    // inform UI that local player has entered combat
    public static UnityEvent EnteredCombat = new UnityEvent();

    // inform UI that local player has left combat
    public static UnityEvent LeftCombat = new UnityEvent();

    // inform UI that local player has moved
    public static UnityEvent ChangedLocation = new UnityEvent();

    // inform UI that local player status has changed (health, exposure, hunger, etc...)
    public static UnityEvent ModifiedStatus = new UnityEvent();

    // inform UI that local player has gained or lost items
    public static UnityEvent ModifiedInventory = new UnityEvent();


    // inform UI that an item in the backpack/3d inventory has been clicked
    public static StringEvent ClickedBagItem = new StringEvent();


    public static LocationEvent OpenedStash = new LocationEvent();


    // add a message to combat text box
    public static StringEvent CombatMessage = new StringEvent();



    // open action menu (location context menu)
    public static UnityEvent ClickedLocation = new UnityEvent();

    // close context menu
    public static UnityEvent ClickedNothing = new UnityEvent();

    // open PVP menu
    public static PlayerEvent ClickedBot = new PlayerEvent();




    // UI Publishes

    public static BoardActionEvent ChoseGameAction = new BoardActionEvent();

    public static StringEvent ChoseCombatAction = new StringEvent();






}

