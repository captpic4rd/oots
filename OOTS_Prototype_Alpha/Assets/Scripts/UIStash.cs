﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UIStash : MonoBehaviour {

    PlayerCore localPlayer;

    List<string> openStash;

    GameObject inventoryPanel; //temp hide inventory while stash is open


    void Awake() {
        localPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCore>();
        //print("UIStash: caching player: " + localPlayer.name + localPlayer.characterName);

        inventoryPanel = GameObject.Find("Inventory Panel"); //temp hide inventory while stash is open
    }

    void Start() { }
    void Update() { }

    void OnEnable() {
        openStash = localPlayer.currentLocation.stash;
        UpdateContainerDisplay();

        OotsEvents.ClickedBagItem.AddListener(OnClickedItem);

        inventoryPanel.SetActive(false); //temp hide inventory while stash is open
    }

    void OnDisable() {
        OotsEvents.ClickedBagItem.RemoveListener(OnClickedItem);

        inventoryPanel.SetActive(true); //temp hide inventory while stash is open
    }



    void OnClickedItem(string itemString) {
        print("UIStash: clicked an item with stash view open" + itemString);

        char[] x = new char[] { ' ' };
        string[] splits = itemString.Split(x);
        string tag = splits[0];
		string item = splits[1];
		if (splits.Length == 3) {
			item = splits [1] + " " + splits [2];
		}
		if (splits.Length == 4) {
			item = splits [1] + " " + splits [2] + " " + splits [3];
		}

        if (tag == "STASH_RIGHT") {
            print("stash right");
            openStash.Add(item);
            localPlayer.inventory.Remove(item);
        }
        else if (tag == "STASH_LEFT") {
            print("stash left");
            openStash.Remove(item);
            localPlayer.inventory.Add(item);
        }

        UpdateContainerDisplay();

        OotsEvents.ModifiedInventory.Invoke();
    }


    [SerializeField]
    GameObject baseItemPrefab;

    void UpdateContainerDisplay() {

        // Populate Inventory Panel
        GameObject inventoryPanel = GameObject.Find("Stash Player Inventory Panel");
        foreach (Transform item in inventoryPanel.transform) {
            Destroy(item.gameObject);
        }

        foreach (var item in localPlayer.inventory) {
            GameObject baseIcon = Instantiate(baseItemPrefab);
            GameObject iconModel = Instantiate(App.itemData[item].UIprefab);
            iconModel.transform.SetParent(baseIcon.transform, false);
            baseIcon.transform.SetParent(inventoryPanel.transform, false);

            var clickScript = baseIcon.GetComponent<OnClickTrigger>();
            clickScript.SetItemName("STASH_RIGHT " + item);
        }


        // Populate Stash Panel
        GameObject stashPanel = GameObject.Find("Stash Panel");
        foreach (Transform item in stashPanel.transform) {
            Destroy(item.gameObject);
        }

        foreach (var item in openStash) {
            GameObject baseIcon = Instantiate(baseItemPrefab);
            GameObject iconModel = Instantiate(App.itemData[item].UIprefab);
            iconModel.transform.SetParent(baseIcon.transform, false);
            baseIcon.transform.SetParent(stashPanel.transform, false);


            var clickScript = baseIcon.GetComponent<OnClickTrigger>();
            clickScript.SetItemName("STASH_LEFT " + item);
        }

    }


    /*
    // actual item xfer handled directly in OnClickedItem at the moment - but ideally the interface wouldn't handle game logic
    // I'm not sure about keeping BoardAction around. Maybe just call localPlayer.GiveItem etc

    public void OnStowItem(string item) {
        BoardAction ba = new BoardAction();
        ba.actionType = ActionType.StowItem;
        ba.item = item;
        OotsEvents.ChoseGameAction.Invoke(ba);
    }


    public void OnTakeItem(string item) {
        BoardAction ba = new BoardAction();
        ba.actionType = ActionType.TakeItem;
        ba.item = item;
        OotsEvents.ChoseGameAction.Invoke(ba);
    }
    */

}
