﻿using UnityEngine;
using System.Collections;
using System;



[Serializable]
public struct CardData {
    public string cardType;

    public string name;
    public string description;
    public string flavorText;

    public int attackPower;
    public int health;
    public int armor;

    public int lootCount;
    public string lootItem;
    //public ItemType lootItem;

    public int encounterTier;

    //public int abilities;
    //public int personality;

    [NonSerialized]
    public Texture frontTexture;


    //[NonSerialized]
    //public Material frontMaterial;
    //[NonSerialized]
    //public Material backMaterial;

}



public enum ItemType { Berry, Crystal, Herb, Wood, Count }


public struct ItemData {

    public string name;
    public string description;

    // type, effects description, isIdentified

    public readonly GameObject UIprefab;

    // effect functor or other specifier


    public bool Use(PlayerCore target) {
        return false;
    }


    public ItemData(string _name, GameObject prefab) {
        name = _name;
        description = "";
        UIprefab = prefab;
    }



}
















