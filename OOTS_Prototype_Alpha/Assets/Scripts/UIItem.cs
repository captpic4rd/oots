﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIItem : MonoBehaviour {

    private string item;

    [SerializeField]
    private Text itemNameDisplayText;

    // temp
    [SerializeField]
    private GameObject crystal;
    [SerializeField]
    private GameObject berry;
    [SerializeField]
    private GameObject herb;
    [SerializeField]
    private GameObject wood;
	[SerializeField]
	private GameObject meat;
	[SerializeField]
	private GameObject cookedMeat;


    public void SetItem(string i) {
        item = i;
        OnEnable();
    }


    void OnEnable() {
        itemNameDisplayText.text = item;

        // temp
        crystal.SetActive(false);
        berry.SetActive(false);
        herb.SetActive(false);
        wood.SetActive(false);
		meat.SetActive (false);
		cookedMeat.SetActive (false);

        if (item == "Crystal") crystal.SetActive(true);
        if (item == "Berry") berry.SetActive(true);
        if (item == "Herb") herb.SetActive(true);
        if (item == "Wood") wood.SetActive(true);
		if (item == "Meat") meat.SetActive (true);
		if (item == "Cooked Meat") cookedMeat.SetActive (true);

		print ("ITEM: " + item);
    }

    void OnDisable() { }

    /*
    public void OnCancelButton() { // TODO: handle in manager so menu state can be managed
        gameObject.SetActive(false);
    }
    */

    public void OnUseButton() {
        BoardAction ba = new BoardAction();
        ba.actionType = ActionType.UseItem;
        ba.item = item;
        OotsEvents.ChoseGameAction.Invoke(ba);

        gameObject.SetActive(false);
    }




}
