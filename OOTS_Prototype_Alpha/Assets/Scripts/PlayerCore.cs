﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Random = UnityEngine.Random;


[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(PlayerStatus))]
public class PlayerCore : MonoBehaviour {
    // global class refs
    public GameManager GM;
    private BattleManager BM;

    // player component refs
    public PlayerStatus status;

    // player info
    public string characterName = "";
    public bool isLocalPlayer = false;

    // state
    public bool isMyTurn = false;
    public bool isInCombat = false;
    public string combatType = "";

    public Location currentLocation = null;

    // inventory
    public List<string> inventory = new List<string>();
    public const int inventoryCapacity = 6;

    // Status
    //const int exposureWarningLevel = 20;
    //const int hungerWarningLevel = 20;
    //const int tirednessWarningLevel = 20; // sleep deprivation

	private string attemptedAction = "";

    // MonoBehaviour methods //////////////////////////////////////////////////////////////////////////////////////////

    private void Awake() {
        GM = FindObjectOfType<GameManager>();
        BM = FindObjectOfType<BattleManager>();
        status = GetComponent<PlayerStatus>();
    }

    private void OnDestroy() {
        if (currentLocation != null) {
            currentLocation.GetComponent<LocationPlatform>().RemoveTokenFromPlatform(gameObject);
        }
        GM.PlayerDeath(gameObject);
    }

    public void InitializeHumanPlayer(string character) {
        isLocalPlayer = true;
        characterName = character;

        var card = App.cardData[characterName];
        status.currentHealth = card.health;
        status.maximumHealth = card.health;

        gameObject.AddComponent<PlayerDragger>();
        gameObject.AddComponent<PlayerHumanController>();

        PlaceAtStartLocation();
        OotsEvents.PlayerSpawned.Invoke();

		OotsEvents.GotInjuryDieResult.AddListener (OnGotInjuryDieResult);
		OotsEvents.GotExposureDieResult.AddListener (OnGotExposureDiceResult);
		OotsEvents.DarkNightEvent.AddListener (OnDarkNightEvent);
    }

    public void InitializeBot(string character) {
        characterName = character;

        var card = App.cardData[characterName];
        status.currentHealth = card.health;
        status.maximumHealth = card.health;

        gameObject.AddComponent<PlayerAIController>();

        PlaceAtStartLocation();
    }

    private void PlaceAtStartLocation() {
        GameObject cabinPlatform = GameObject.Find("Cabin Platform");
        if (cabinPlatform) {
            MoveToPlatform(cabinPlatform.GetComponent<Location>());
        }
        else {
            Debug.Log("Cabin Platform not found");
        }
    }
		
    // Board Actions //////////////////////////////////////////////////////////////////////////////////////////////////

    public void ActionUse(string item) {
        Debug.Log("ActionUse(" + item + ")");
		if (item == "Berry") {
			if (inventory.Contains ("Berry")) {
				inventory.Remove ("Berry");

				status.ModifyHunger (1);
			}
		} else if (item == "Herb") {
			if (inventory.Contains ("Herb")) {
				inventory.Remove ("Herb");

				status.ModifyHealth (5, false);

				status.SetInjured (false);
			}
		} else if (item == "Meat") {
			if (inventory.Contains ("Meat")) {
				if (currentLocation.isShelter && currentLocation.HasFire ()) {
					inventory.Remove ("Meat");
					inventory.Add ("Cooked Meat");
				}
			}
		} else if (item == "Wood") {
			if (inventory.Contains ("Wood")) {
				if (currentLocation.isShelter) {
					inventory.Remove ("Wood");
					currentLocation.UseWood ();
				}
			}
		} else if (item == "Cooked Meat") {
			if (inventory.Contains ("Cooked Meat")) {
				inventory.Remove ("Cooked Meat");
				status.ModifyHunger (2);
			}
		}
        /*
        else {
            _DevUI.TextOut("PlayerCore.ActionUse: No implementation for Use(" + item + ")");
        }
        */

        if (isLocalPlayer) OotsEvents.ModifiedInventory.Invoke();
    }

	//If the attempted action was successful (rolled anything but a 1) call the corresponding action
	private void OnGotInjuryDieResult(int result){
		if (result != 1) {
			if (attemptedAction == "gather") {
				ActionGather ();
			}
			if (attemptedAction == "study") {
				ActionStudy ();
			}
			if (attemptedAction == "hunt") {
				ActionHunt ();
			}
			if (attemptedAction == "rest") {
				ActionRest ();
			}
		} else {
			Debug.Log ("Action failed due to injury");
			StartEndTurn ();
		}

		attemptedAction = "";
	}

	//attempt to gather. If not injured, always succeeds.
	public void AttemptGather(){
		if (status.isInjured) {
			attemptedAction = "gather";
			OotsEvents.AttemptedActionWhileInjured.Invoke();
		} else {
			ActionGather ();
		}
	}

    public void ActionGather() { //ActionGather(Location location)
        Debug.Log("gathering from " + currentLocation);

        string resource = currentLocation.GatherResource();

        if (resource == null) {
            Debug.Log("location is out of resources");
        }
        else {
            Debug.Log("gathered " + resource + " x 1");
            //inventory.Add(resource);
            GiveItem(resource, 1);

            OotsEvents.ModifiedInventory.Invoke();
        }
        StartEndTurn();
    }

    public void ActionStowAll() {
        foreach (var item2 in inventory) {
            currentLocation.Stow(item2);
        }
        inventory.Clear();
        OotsEvents.ModifiedInventory.Invoke();
    }

    public void ActionTakeAll() {
        inventory.AddRange(currentLocation.TakeAll());
        OotsEvents.ModifiedInventory.Invoke();
    }
	public void AttemptStudy(){
		if (status.isInjured) {
			attemptedAction = "study";
			OotsEvents.AttemptedActionWhileInjured.Invoke ();
		} else {
			ActionStudy ();
		}
	}
		
    public void ActionStudy() {
        Debug.Log("studying for a turn..");
        _DevUI.TextOut("studying for a turn..");
        StartEndTurn();
    }

	public void AttemptRest(){
		if (status.isInjured) {
			attemptedAction = "rest";
			OotsEvents.AttemptedActionWhileInjured.Invoke ();
		} else {
			ActionRest ();
		}
	}

    public void ActionRest() {
        Debug.Log("resting for a turn..");
        _DevUI.TextOut("resting for a turn..");
        //status.tiredness = 0;
        status.SetTiredness(0);
        status.ModifyHealth(1, false);
		if (currentLocation.isShelter && currentLocation.HasFire()) {
			status.ModifyExposure (0, true);
		}
        StartEndTurn();
    }

    public void ActionSteal(int who) {
		if (InjuryFail ()) {
			StartEndTurn ();
			return;
		}
    }

    public void ActionAttack(PlayerCore target) {
		if (InjuryFail ()) {
			StartEndTurn ();
			return;
		}
        BM.StartBattleWithPlayer(this, target);
        EnterCombat("attack");
    }

	public void AttemptHunt(){
		if (isLocalPlayer) {
			if (status.isInjured) {
				attemptedAction = "hunt";
				OotsEvents.AttemptedActionWhileInjured.Invoke ();
			}
			if(!status.isInjured){
				ActionHunt ();
			}
		}

		if (!isLocalPlayer) {
			if (status.isInjured) {
				if (!InjuryFail ()) {
					ActionHunt ();
				}
			} else {
				ActionHunt ();
			}
		}
	}

    public void ActionHunt() {
        BM.StartBattleWithMonster(this, currentLocation.region);
        EnterCombat("hunt");
    }


    // Public methods /////////////////////////////////////////////////////////////////////////////////////////////////

    public int GiveItem(string item, int count) {
        int given = 0;
        while (given < count) {
            if (inventory.Count >= inventoryCapacity)
                break;
            inventory.Add(item);
            given++;
        }
        if (given > 0 && isLocalPlayer) {
            OotsEvents.ModifiedInventory.Invoke();
        }
        return given;
    }

    public void StartTurn() { // GameManager calls this, then waits on isMyTurn
        isMyTurn = true;

        // weather effect logic


        
        status.ModifyHunger(-1);
        
        if (isLocalPlayer) OotsEvents.StartedTurn.Invoke();
    }

	//I changed the name of this function from EndTurn to StartEndTurn because, if the player needs to roll exposure dice,
	//this function will end without the turn ending (it ends in the OnGotExposureDiceResult() function),
	//so it's more like it is beginning the end turn process. 
    public void StartEndTurn() {
		if (!currentLocation.isShelter) {
			if (GM.weather == Weather.Clear) {
				status.ModifyExposure (0, false);
			} else if (GM.weather == Weather.Hail) {
				status.ModifyExposure (1, false);
			} else if (GM.weather != Weather.Snow) {
				status.ModifyExposure (1, false);
			} else if (GM.weather != Weather.Blizzard) {
				status.ModifyExposure (1, false);
			}
		}

		if (GM.phase == Phase.Night) {
			_DevUI.TextOut ("PC: Applying hunger... ");
			ApplyHungerEffect ();
		}

		if (GM.phase == Phase.Night && status.exposure > 0) {
			if (isLocalPlayer) {
				LocalPlayerExposure ();
			} else {
				AIExposure();
				isMyTurn = false;
			}
		}

		if (GM.phase == Phase.Night && status.exposure == 0) {
			isMyTurn = false;
			if (isLocalPlayer) {
				OotsEvents.EndedTurn.Invoke ();
			}
		}

		if (GM.phase != Phase.Night) {
			isMyTurn = false;
			if (isLocalPlayer)
				OotsEvents.EndedTurn.Invoke ();
		}
    }

	void ApplyHungerEffect(){
		if (status.hunger == 0) {
			Debug.Log ("PC: Took 1 damage from hunger.");
			status.ModifyHealth (-1, false);
			if (status.currentHealth == 0 && !status.isInjured) {
				status.SetInjured (true);
			}
			else if (status.currentHealth == 0 && status.isInjured) {
				Die ();
			}
		}
	}

	void OnGotExposureDiceResult(bool injured){
		Debug.Log ("exposure ending turn");
		if (injured == true) {
			status.SetInjured (true);
		}
		isMyTurn = false;
		if (isLocalPlayer)
			OotsEvents.EndedTurn.Invoke ();
	}

	void LocalPlayerExposure(){
		int numExposureDice = status.exposure;
		OotsEvents.EndingTurnAtNightWithExposure.Invoke (numExposureDice);
	}

	void AIExposure(){
		for (int i = 0; i < status.exposure; i++) {
			int roll = Random.Range (1, 6);
			if (roll >= 4) {
				status.SetInjured (true);
				return;
			}
		}
	}

    public void Die() {
        if (isInCombat)
            DepartCombat();

        if (isLocalPlayer) {
            // TODO: set game stats in App (days survived etc)
            App.SwitchScene("Game Over Menu");
        }
        else {
            _DevUI.TextOut(characterName + " died");
            Destroy(gameObject);
        }
    }

    public void EnterCombat(string type) {
        isInCombat = true;  // TODO: replace these two state variables with enum
        combatType = type;

        Debug.Log("PlayerCore.EnterCombat: " + characterName + " has started combat");

        //BM.StartBattle    // TODO: would make some sense to move StartBattleXXX commands here instead of always calling StartBattleXXX and then EnterCombat

        if (isLocalPlayer) OotsEvents.EnteredCombat.Invoke();
    }

    public void DepartCombat() {
        isInCombat = false;

        Debug.Log("PlayerCore.DepartCombat: " + characterName + " has ended combat: " + combatType);

        if (isLocalPlayer) OotsEvents.LeftCombat.Invoke();

        if (combatType == "hunt")
            StartEndTurn();
        else if (combatType == "attack")
            StartEndTurn();
        else if (combatType == "encounter")
            StartEndTurn();
    }

    public List<string> GetAvailableActions() { // used by UI manager to construct action menu - could also be used by AI
        List<string> actions = new List<string>(); // TODO: use BoardActions enum

        if (currentLocation.canGather) { actions.Add("Gather"); }
        if (currentLocation.canHunt) { actions.Add("Hunt"); }
        if (currentLocation.canStash) { actions.Add("Stash"); }
        if (currentLocation.firepit != null) { actions.Add("Fire"); }
        actions.Add("Study"); // if cards left in journal card deck
        actions.Add("Rest"); // hubs only?

        var platform = currentLocation.platform;

        if (platform.tokensOnPlatform.Count > 1) {
            //actions.Add("Attack");
            //actions.Add("Steal");
        }

        return actions;
    }

    public List<GameObject> GetNearbyPlayers() {
        List<GameObject> players = new List<GameObject>(currentLocation.platform.tokensOnPlatform);
        players.Remove(gameObject);
        return players;
    }

    public void MoveToPlatform(Location newLocation) {
        if (currentLocation == newLocation) {
            currentLocation.platform.AdjustTokenPositions();
            return;
        }

        // Don't perform region or encounter checks if this is the initial token placement
        if (currentLocation == null) {
            newLocation.platform.PlaceTokenOnPlatform(gameObject);
            currentLocation = newLocation;
            return;
        }

        int newRegion = newLocation.region;
        int currentRegion = currentLocation.region;

        currentLocation.platform.RemoveTokenFromPlatform(gameObject);

        // Move back to starting platform if trying to move too far
        if (Math.Abs(newRegion - currentRegion) > 1) {
            currentLocation.platform.PlaceTokenOnPlatform(gameObject);
            if (isLocalPlayer) Debug.Log("Attempted to move too far, returning to starting platform");
            return;
        }
        else {
            newLocation.platform.PlaceTokenOnPlatform(gameObject);
            currentLocation = newLocation;
			//check for the Abandoned Wagon event
			if (currentLocation.WagonPresent ()) {
				inventory.Add ("Berry");
				inventory.Add ("Cooked Meat");
				inventory.Add ("Wood");
				if (isLocalPlayer) OotsEvents.ModifiedInventory.Invoke();
			}
        }

        if (isLocalPlayer) {
            if (currentRegion != newRegion) {

                OotsEvents.ChangedRegion.Invoke(newRegion);

                if (Random.Range(0, 100) < 50) {
                    BM.StartBattleWithMonster(this, currentLocation.region);
                    EnterCombat("encounter");
                }
                else {
                    StartEndTurn();
                }
            }
            else {
                OotsEvents.ChangedLocation.Invoke();
            }
        }
        else {
            StartEndTurn();
        }
    }

	void OnDarkNightEvent(){
		if (isLocalPlayer) {
			if (!currentLocation.isShelter) {
				OotsEvents.GotDarkNightRollResults.AddListener (GotDarkNightRollResults);
				OotsEvents.DoDarkNightRoll.Invoke ();
			}
		} else {
			if (!currentLocation.isShelter) {
				int d4 = Random.Range (1, 4);
				if (d4 == 4) {
					if (status.isInjured) {
						Die ();
					} else {
						status.SetInjured (true);
					}
				}
			}
		}
	}

	void GotDarkNightRollResults(int rollResult){
		OotsEvents.GotDarkNightRollResults.RemoveListener (GotDarkNightRollResults);
		if (true) {
			if (status.isInjured) {
				Die ();
			} else {
				status.SetInjured (true);
			}
		}
	}

	bool InjuryFail(){
		if (status.isInjured) {
			float num = Random.Range (1, 4);
			if (num == 1) {
				Debug.Log ("Failed to act due to injury");
				return true;
			}
		}
		return false;
	}
}
