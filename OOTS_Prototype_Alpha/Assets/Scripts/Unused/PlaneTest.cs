﻿using UnityEngine;
using System.Collections;

public class PlaneTest : MonoBehaviour {

    private Plane hudPlane;
    private Camera hudCam;


    void Awake() {
        hudCam = GetComponentInParent<Camera>();

        hudPlane = new Plane(Vector3.back, transform.position);

    }

    void Start() {
    }

    void Update() {
        if (Input.GetMouseButtonDown(0)) {
            Ray ray = hudCam.ScreenPointToRay(Input.mousePosition);

            float dist;
            Vector3 hitPoint = Vector3.zero;
            if (hudPlane.Raycast(ray, out dist)) {
                hitPoint = ray.GetPoint(dist);
                //print("invisible plane clicked" + Input.mousePosition.x + "," + Input.mousePosition.y);
                //print(hitPoint + " " + dist);

                var go = GameObject.Find("Backpack");
                go.transform.position = hitPoint;


            }
            else {
                print("hudPlane not hit");
            }

        }
    }

    private void OnMouseDown() {
        
        //print("blue plane clicked" + Input.mousePosition.x + "," + Input.mousePosition.y);

        /*
        Ray ray = hudCam.ScreenPointToRay(Input.mousePosition);

        float dist;
        Vector3 hitPoint = Vector3.zero;
        if (hudPlane.Raycast(ray, out dist)) {
            hitPoint = ray.GetPoint(dist);
            print(hitPoint);
        }
        else {
            print("hudPlane not hit");
        }
        */



    }

}
