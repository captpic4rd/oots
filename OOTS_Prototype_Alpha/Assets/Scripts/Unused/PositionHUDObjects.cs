﻿using UnityEngine;
using System.Collections;

public class PositionHUDObjects : MonoBehaviour {

    //private Plane hudPlane;
    private Camera hudCamera;
    private Transform backpackTransform;

    Vector3 point;

    void Awake() {
        hudCamera = GetComponent<Camera>();
        //hudPlane = new Plane(Vector3.back, new Vector3(transform.position.x, transform.position.y, 4f));
        backpackTransform = GameObject.Find("Backpack").GetComponent<Transform>();
        InvokeRepeating("Reposition3dHud", 0, .2f);
    }

    void Reposition3dHud() {
        point = hudCamera.ScreenToWorldPoint(new Vector3(Screen.width - 50, 20, 4.0f));
        backpackTransform.position = point;
    }

    /*
    private int width, height;

    width = Screen.width;
        height = Screen.height;

        InvokeRepeating("CheckResolutionChanged", 0, .2f);

    void CheckResolutionChanged() {
        if (!(Screen.width == width) || !(Screen.height == height)) {
            print("screen dimensions changed");
            width = Screen.width;
            height = Screen.height;
            Reposition3dHud(width);
        }

    }

    void Reposition3dHud(int _width) {
        var p = hudCam.ScreenToWorldPoint(new Vector3(_width - 50, 20, 4.0f));
        backpackTransform.position = p;
    }
    */

}