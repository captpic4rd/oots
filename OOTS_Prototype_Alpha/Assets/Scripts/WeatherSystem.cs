﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


public class WeatherSystem : MonoBehaviour {
    private GameManager GM;

    [SerializeField]
    private ParticleSystem snow;
    [SerializeField]
    private ParticleSystem hail;
    [SerializeField]
    private ParticleSystem blizzard;

    private Dictionary<Weather, ParticleSystem> weatherEffectMapping;

    private Weather currentWeatherEffect = Weather.Clear;

    void Awake() {
        GM = FindObjectOfType<GameManager>();

        weatherEffectMapping = new Dictionary<Weather, ParticleSystem>() {
            { Weather.Snow, snow },
            { Weather.Hail, hail },
            { Weather.Blizzard, blizzard },
			{ Weather.Blizzard2, blizzard },
			{ Weather.Blizzard3, blizzard },
			{ Weather.Blizzard4, blizzard }
        };
    }

    void Update() {
        if (currentWeatherEffect != GM.weather) {
            StopWeatherEffect(currentWeatherEffect);
            StartWeatherEffect(GM.weather);
            currentWeatherEffect = GM.weather;
        }
    }

    void StartWeatherEffect(Weather weather) {
        if (weather != Weather.Clear) {
            weatherEffectMapping[weather].Play();
        }
    }

    void StopWeatherEffect(Weather weather) {
        if (weather != Weather.Clear) {
            weatherEffectMapping[weather].Stop();
        }
    }

    /*
    void StopCurrentWeatherEffect() {
        weatherEffectMapping[currentWeatherEffect].Stop();
        currentWeatherEffect = Weather.Clear;
    }
    */

}
