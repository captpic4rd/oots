﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    void Update() {
        // Key scrolling
        var x = Input.GetAxis("Horizontal") * Time.deltaTime * 100.0f;
        var z = Input.GetAxis("Vertical") * Time.deltaTime * 100.0f;
        transform.Translate(new Vector3(x, 0, z), Space.World);

        // Zoom in/out with mousewheel
        var zoom = Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * 1000.0f;
        transform.Translate(0, 0, zoom);

        // Edge scrolling in fullscreen
        if (Screen.fullScreen) {
            if (Input.mousePosition.x <= 0) {
                transform.Translate(new Vector3(-1 * Time.deltaTime * 100.0f, 0, 0), Space.World);
            }
            else if (Input.mousePosition.x >= (Screen.currentResolution.width - 1)) {
                transform.Translate(new Vector3(1 * Time.deltaTime * 100.0f, 0, 0), Space.World);
            }

            if (Input.mousePosition.y <= 0) {
                transform.Translate(new Vector3(0, 0, -1 * Time.deltaTime * 100.0f), Space.World);
            }
            else if (Input.mousePosition.y >= (Screen.currentResolution.height - 1)) {
                transform.Translate(new Vector3(0, 0, 1 * Time.deltaTime * 100.0f), Space.World);
            }
        }

        /*
                if (zoom > 0f)
                {
                    transform.Translate(0, 0, Time.deltaTime * 100.0f);
                }
                else if (zoom < 0f)
                {
                    transform.Translate(0, 0, Time.deltaTime * -100.0f);
                }*/

        /*
        transform.Rotate()

        if (Input.GetKey(KeyCode.PageDown)) {

        }
        else if (true) {

        }
        if (Input.GetKey(KeyCode.Home)) {

        }
        */
    }
}
