﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using Random = UnityEngine.Random;

using UnityEngine.EventSystems;



public class PlayerAIController : MonoBehaviour, IPointerClickHandler {
    PlayerCore playerCore;

    void Awake() {
        playerCore = GetComponent<PlayerCore>();

    }

    void Start() { }

    void OnDestroy() { }


    void FixedUpdate() {
        if (!playerCore.isMyTurn)
            return;

        if (playerCore.isInCombat)
            return;
        //playerCore.GetNearbyPlayers();

		List<string> inventory = playerCore.inventory;
		bool isInjured = playerCore.status.isInjured;
		int hunger = playerCore.status.hunger;
		Phase phase = playerCore.GM.phase;

		if (isInjured && inventory.Contains ("Herb")) {
			playerCore.ActionUse ("Herb");
			_DevUI.TextOut (playerCore.characterName + " used Herb");
		}

		if (hunger < 6 && inventory.Contains ("Berry")) {
			playerCore.ActionUse ("Berry");
			_DevUI.TextOut (playerCore.characterName + " ate Berry");
		}

		if (phase == Phase.Night && playerCore.currentLocation.isShelter) {
			if (!playerCore.currentLocation.HasFire ()) {
				if (inventory.Contains ("Wood")) {
					playerCore.ActionUse ("Wood");
					_DevUI.TextOut (playerCore.characterName + " started a fire");
				}
			}
		}

		if (phase == Phase.Night && playerCore.currentLocation.isShelter) {
			playerCore.AttemptRest ();
			_DevUI.TextOut(playerCore.characterName + " is resting");
			return;
		}

        if (playerCore.currentLocation.canHunt && Random.Range(0, 100) < 50) {
			_DevUI.TextOut(playerCore.characterName + " is hunting");
            playerCore.AttemptHunt();
            return;
        }

		if (playerCore.currentLocation.canGather && Random.Range (0, 100) < 50) {
			_DevUI.TextOut(playerCore.characterName + " is gathering");
			playerCore.AttemptGather ();
			return;
		}

		if (phase == Phase.Night) {
			Location neighbor = playerCore.currentLocation.neighbors [0];
			if (neighbor != null) {
				if (neighbor.isShelter) {
					playerCore.MoveToPlatform (neighbor);
				}
			}
		} else {
			var allLocations = FindObjectsOfType<Location> ();
			var moveTo = allLocations [Random.Range (0, allLocations.Length)];
			playerCore.MoveToPlatform (moveTo);
		}
        // currently AI keeps trying to move to a random platform until one is within range

        //var actions = playerCore.GetAvailableActions();


        //Debug.Log("AI moving to platform: " + moveTo);


    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        OotsEvents.ClickedBot.Invoke(playerCore);
		_DevUI.TextOut (playerCore.characterName + "hp: " + playerCore.status.currentHealth + ", hunger: " + playerCore.status.hunger + ", injured: " + playerCore.status.isInjured);
		string inventoryString = "Inventory: ";
		for (int i = 0; i < playerCore.inventory.Count; i++) {
			inventoryString += playerCore.inventory [i] + ", ";
		}
		_DevUI.TextOut (inventoryString);

    }
		
    // TEMP Fog of War stuff - should be moved to fogofwarsystem

    RectTransform uiCanvasRectTransform;
    RectTransform injuryImageRectTransform;
    GameObject injuryImage;
    Transform overheadPlacer;
    Transform HUD;

    //GameManager GM;
    PlayerCore localPlayer;
    FogOfWarSystem FOW;

    void Update() {
        //if (GM == null) GM = FindObjectOfType<GameManager>();
        if (localPlayer == null) localPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCore>();
        if (FOW == null) FOW = FindObjectOfType<FogOfWarSystem>();

        if (HUD == null) HUD = GameObject.Find("HUD").GetComponent<Transform>();
        if (overheadPlacer == null) overheadPlacer = transform.FindChild("Overhead Menu Placer");
        if (injuryImage == null) {
            injuryImage = Instantiate(Resources.Load<GameObject>("Injury Image"));
            injuryImage.transform.SetParent(HUD, false);
            injuryImageRectTransform = injuryImage.GetComponent<RectTransform>();
        }
        if (uiCanvasRectTransform == null) uiCanvasRectTransform = GameObject.Find("UI Canvas").GetComponent<RectTransform>();

        if (playerCore.status.isInjured) {
            if (localPlayer.currentLocation.region != playerCore.currentLocation.region && FOW.gameObject.activeSelf) {
                injuryImage.SetActive(false);
            }
            else {
                float xScaler = uiCanvasRectTransform.rect.width / Screen.width;
                float yScaler = uiCanvasRectTransform.rect.height / Screen.height;
                Vector3 screenPointOfObjectCenter = Camera.main.WorldToScreenPoint(overheadPlacer.position); //localPlayer.transform.position
                injuryImageRectTransform.anchoredPosition = new Vector3(screenPointOfObjectCenter.x * xScaler, screenPointOfObjectCenter.y * yScaler, 0);
                injuryImage.SetActive(true);
            }
        }
        else {
            injuryImage.SetActive(false);
        }
    }


}
