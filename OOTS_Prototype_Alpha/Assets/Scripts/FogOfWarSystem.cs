﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;
using System.Collections;


public class FogOfWarSystem : MonoBehaviour {
    GameManager GM;
    PlayerCore localPlayer;

    public GameObject[] planes = new GameObject[7];
    //public List<GameObject> planes = new List<GameObject>();

    void Awake() {
        localPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCore>();
        GM = FindObjectOfType<GameManager>();
    }

    void Start() {
        InvokeRepeating("UpdatePlayerVisibility", 0.1f, 0.1f);
    }

    void OnEnable() {
        foreach (GameObject plane in planes) {
            plane.SetActive(true);
        }

        if (localPlayer.currentLocation != null) {
            planes[localPlayer.currentLocation.region - 1].SetActive(false);
        }
        else {
            planes[0].SetActive(false);
        }

        OotsEvents.ChangedRegion.AddListener(OnChangedRegion);
    }

    void OnDisable() {
        foreach (GameObject plane in planes) {
            plane.SetActive(false);
        }

        OotsEvents.ChangedRegion.RemoveListener(OnChangedRegion);
    }

    public void OnChangedRegion(int newRegion) {
        //Debug.Log("FogManager: RegionChanged to " + newRegion.ToString());

        foreach (GameObject plane in planes) {
            plane.SetActive(true);
        }
        planes[newRegion - 1].SetActive(false);
    }


    public void OnToggleButton() {
        if (gameObject.activeSelf) {
            gameObject.SetActive(false);
        }
        else {
            gameObject.SetActive(true);
        }

    }
    
    // Shadows of the Unknown - hide players not in same region
    public void UpdatePlayerVisibility() {
        if (!gameObject.activeSelf) {
            foreach (var p in GM.players) {
                p.GetComponentInChildren<MeshRenderer>().enabled = true;
            }
            return;
        }
        
        int currentRegion = localPlayer.GetComponent<PlayerCore>().currentLocation.region;

        foreach (var p in GM.players) {
            if (p.GetComponent<PlayerCore>().currentLocation.region != currentRegion) {
                p.GetComponentInChildren<MeshRenderer>().enabled = false;
            }
            else {
                p.GetComponentInChildren<MeshRenderer>().enabled = true;
            }
        }











    }
}
