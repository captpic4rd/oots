﻿Shader "GUI/TextShader1" {
	Properties{
		_MainTex("Font Texture", 2D) = "white" { }
		_Color("Text Color", Color) = (1,1,1,1)

		//_MainTex2("Font Texture", 2D) = "white" { }
		//_Color2("Text Color", Color) = (1,1,1,1)
	}

	SubShader{
		//Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		Lighting Off
		Cull Back
		ZTest LEqual
		ZWrite Off
		Offset -1, -1
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha

		//ColorMask G

		Pass {
			Color(0.0,0.5 ,0.0,1)
			SetTexture[_MainTex] {
				combine primary, texture * primary
			}
		}

		/*
		Pass {
			Color[_Color]
			SetTexture[_MainTex] {
				//combine primary, texture * primary
			}
		}
			*/
		//Pass {



		//}



			/*
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_particles

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			//fixed4 _TintColor;
			float _Scale;

			struct appdata_t {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : POSITION;
				fixed4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				//#ifdef SOFTPARTICLES_ON
				//float4 projPos : TEXCOORD1;
				//#endif
			};

			float4 _MainTex_ST;

			v2f vert(appdata_t v) {
				v2f o;
				o.vertex = mul(UNITY_MATRIX_P,
				mul(UNITY_MATRIX_MV, float4(0.0, 0.0, 0.0, 1.0)) +
				float4(v.vertex.x, v.vertex.y, 0.0, 0.0));
				//#ifdef SOFTPARTICLES_ON
				//o.projPos = ComputeScreenPos (o.vertex);
				//COMPUTE_EYEDEPTH(o.projPos.z);
				//#endif
				o.color = v.color;
				o.texcoord = TRANSFORM_TEX(v.texcoord,_MainTex);
				return o;
			}

			//sampler2D _CameraDepthTexture;
			//float _InvFade;

			fixed4 frag(v2f i) : COLOR {
				//#ifdef SOFTPARTICLES_ON
				//float sceneZ = LinearEyeDepth (UNITY_SAMPLE_DEPTH(tex2Dproj(_CameraDepthTexture, UNITY_PROJ_COORD(i.projPos))));
				//float partZ = i.projPos.z;
				//float fade = saturate (_InvFade * (sceneZ-partZ));
				//i.color.a *= fade;
				//#endif

				return i.color * tex2D(_MainTex, i.texcoord) * i.color.a;
			}
			ENDCG
		}*/
	}

	Fallback "TextShader"
}