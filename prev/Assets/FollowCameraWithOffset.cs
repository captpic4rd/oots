﻿using UnityEngine;
using System.Collections;

public class FollowCameraWithOffset : MonoBehaviour {

	// Use this for initialization
	private Transform cameraTransform;
	private Vector3 offset;

	void Awake(){
		cameraTransform = GameObject.FindGameObjectWithTag ("MainCamera").GetComponent<Transform> ();
	}

	void Start () {
		offset = cameraTransform.position - transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = cameraTransform.position - offset;
	}
}
