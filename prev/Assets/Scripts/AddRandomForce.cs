﻿using UnityEngine;
using System.Collections;

public class AddRandomForce : MonoBehaviour {

	public float MinForce;
	public float MaxForce;
	public bool RandomRotation;
	private Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();		
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetMouseButtonDown(0)){
			Vector3 randomForce = new Vector3(Random.Range(MinForce, MaxForce), Random.Range(MinForce, MaxForce), Random.Range(MinForce, MaxForce));
			rb.AddForce(randomForce);
			if(RandomRotation){
				rb.AddTorque(randomForce);
			}
		}
	}
}
