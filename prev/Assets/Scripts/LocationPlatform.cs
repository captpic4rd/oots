﻿using UnityEngine;
using System.Collections.Generic;

public class LocationPlatform : MonoBehaviour {

    private const float radius = 2.5f; //1.7f; // 2.0f
    private const int maximumTokens = 3;

    private Vector3[] snapPoints = new Vector3[maximumTokens];

    //private Dictionary<string, Vector3> playerSnapPoints = new Dictionary<string, Vector3>();


    [HideInInspector]
    public List<GameObject> tokensOnPlatform = new List<GameObject>();


    private void Awake() {
        // Generate snapPoints around edge of circle
        for (int i = 0; i < 3; i++) { // snapPoints.Length
            float angle = i * Mathf.PI * 2 / snapPoints.Length;
            snapPoints[i] = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * radius;
            snapPoints[i] += transform.position;
        }
    }

    public void AdjustTokenPositions() {
        for (int i = 0; i < tokensOnPlatform.Count; i++) {
            tokensOnPlatform[i].transform.position = snapPoints[i];
        }
    }

    public void PlaceTokenOnPlatform(GameObject token) {
        tokensOnPlatform.Add(token);
        AdjustTokenPositions();
    }

    public void RemoveTokenFromPlatform(GameObject token) {
        tokensOnPlatform.Remove(token);
        AdjustTokenPositions();
    }
}
