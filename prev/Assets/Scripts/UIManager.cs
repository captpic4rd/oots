﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class UIManager : MonoBehaviour {
    // Sub UIs
    [SerializeField]
    private UICombat uiCombat;
    [SerializeField]
    private UIPvpMenu uiPvpMenu;
    [SerializeField]
    private UIActionMenu uiActionMenu;
    [SerializeField]
    private UIItem uiItem;
    [SerializeField]
    private UIHud uiHud;
    [SerializeField]
    private UIStash uiStash;
    [SerializeField]
    private UIGameMenu uiGameMenu;

    [SerializeField]
    private _DevUI uiDev;


    void Awake() { }


    void OnEnable() {
        //OotsEvents.PlayerSpawned.AddListener(OnPlayerSpawned);
        OotsEvents.EnteredCombat.AddListener(OnEnteredCombat);
        OotsEvents.LeftCombat.AddListener(OnLeftCombat);
        OotsEvents.ClickedBagItem.AddListener(OnClickedItem);
        OotsEvents.OpenedStash.AddListener(OnViewStash);

        OotsEvents.StartedTurn.AddListener(OnPlayerTurnStart);
        OotsEvents.EndedTurn.AddListener(OnPlayerTurnEnd);

        OotsEvents.ClickedLocation.AddListener(OnClickedLocation);
        OotsEvents.ClickedBot.AddListener(OnClickedBot);
    }

    void OnDisable() {
        //OotsEvents.PlayerSpawned.RemoveListener(OnPlayerSpawned);
        OotsEvents.EnteredCombat.RemoveListener(OnEnteredCombat);
        OotsEvents.LeftCombat.RemoveListener(OnLeftCombat);
        OotsEvents.ClickedBagItem.RemoveListener(OnClickedItem);
        OotsEvents.OpenedStash.RemoveListener(OnViewStash);

        OotsEvents.StartedTurn.RemoveListener(OnPlayerTurnStart);
        OotsEvents.EndedTurn.RemoveListener(OnPlayerTurnEnd);

        OotsEvents.ClickedLocation.RemoveListener(OnClickedLocation);
        OotsEvents.ClickedBot.RemoveListener(OnClickedBot);
    }

    void OnClickedLocation() {
        uiActionMenu.gameObject.SetActive(true);
    }

    void OnClickedBot(PlayerCore bot) {
        Debug.Log("clicked bot: " + bot.characterName);
        uiPvpMenu.SetTargetPlayer(bot);
        uiPvpMenu.gameObject.SetActive(true);
    }


    /*
    void OnPlayerSpawned() {
        uiAlways.gameObject.SetActive(true);
        uiNonCombat.gameObject.SetActive(true);
    }
    */


    void OnEnteredCombat() {
        uiCombat.gameObject.SetActive(true);
        //uiNonCombat.gameObject.transform.parent.gameObject.SetActive(false); // TODO: cache this or rearrange hierarchy

        uiDev.gameObject.SetActive(false);

        uiActionMenu.gameObject.SetActive(false);
        uiPvpMenu.gameObject.SetActive(false);
    }

    void OnLeftCombat() {
        uiCombat.gameObject.SetActive(false);
        //uiNonCombat.gameObject.transform.parent.gameObject.SetActive(true); // "

        uiDev.gameObject.SetActive(true);

        uiActionMenu.gameObject.SetActive(true);
    }



    void OnClickedItem(string itemString) {
        if (!uiStash.gameObject.activeSelf) {
            // Stash isn't open so the clicked item must have been an inventory item - open Item View
            OnViewItem(itemString);
        }
        else {
            //print("clicked a stash item " + itemString);
        }
    }


    void OnViewItem(string whichItem) {
        // TODO: check current screen/mode && is player turn (or check turn from within sub-UI to avoid having a link here.. or implement a method in GM to ask whether it is player turn)

        // temp: handle this in SetItem
        //ItemUI.gameObject.SetActive(false);

        uiItem.SetItem(whichItem);
        uiItem.gameObject.SetActive(true);

        uiActionMenu.gameObject.SetActive(false);
        uiPvpMenu.gameObject.SetActive(false);

    }




    public void OnCloseItemDisplay() { // currently UIItem disables itself instead of sending a message asking UIManager to disable it.
        uiItem.gameObject.SetActive(false);
        uiActionMenu.gameObject.SetActive(true);
    }

    void OnViewStash(Location location) {
        uiActionMenu.gameObject.SetActive(false);
        uiStash.gameObject.SetActive(true);

        uiPvpMenu.gameObject.SetActive(false);
    }

    public void OnCloseStash() {
        uiStash.gameObject.SetActive(false);

        //uiActionMenu.gameObject.SetActive(true);
    }


    void OnPlayerTurnStart() {
        if (!uiActionMenu.gameObject.activeSelf) {
            uiActionMenu.gameObject.SetActive(true);
        }

        //uiDev.gameObject.SetActive(true);

    }

    void OnPlayerTurnEnd() {
        if (uiActionMenu.gameObject.activeSelf) {
            uiActionMenu.gameObject.SetActive(false);
        }

        uiPvpMenu.gameObject.SetActive(false);

        //uiDev.gameObject.SetActive(false);
    }

    // Button Handlers ////////////////////////////////////////////////////////////////////////////////////////////////

    public void OnShowMenuButtonPressed() {
        uiGameMenu.gameObject.SetActive(true);
    }

    /*public void OnBlankClick() {
        uiContextMenu.gameObject.SetActive(false);
    }
    */





}
