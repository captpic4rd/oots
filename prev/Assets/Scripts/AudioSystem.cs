﻿using UnityEngine;
using System.Collections;

public class AudioSystem : MonoBehaviour {

    public AudioSource backgroundMusic;

    private void Start() {
//        backgroundMusic = GetComponentInChildren<AudioSource>();
    }

    public void ToggleMusic() {
        if (backgroundMusic.isPlaying) {
            backgroundMusic.Stop();
        }
        else {
            backgroundMusic.Play();
        }
    }

    public void PlaySound() {

    }

}
