﻿using UnityEngine;
using System;
using System.Collections;
using System.Linq;
using Random = UnityEngine.Random;

using UnityEngine.EventSystems;



public class PlayerAIController : MonoBehaviour, IPointerClickHandler {
    PlayerCore playerCore;


    void Awake() {
        playerCore = GetComponent<PlayerCore>();

    }

    void Start() { }

    void OnDestroy() { }


    void FixedUpdate() {
        if (!playerCore.isMyTurn)
            return;

        if (playerCore.isInCombat)
            return;


        //playerCore.GetNearbyPlayers();

        if (playerCore.currentLocation.canHunt && Random.Range(0, 100) < 25) {
            Debug.Log(playerCore.characterName + " is hunting");
            playerCore.ActionHunt();
            return;
        }


        // currently AI keeps trying to move to a random platform until one is within range

        //var actions = playerCore.GetAvailableActions();

        var allLocations = FindObjectsOfType<Location>();
        var moveTo = allLocations[Random.Range(0, allLocations.Length)];
        playerCore.MoveToPlatform(moveTo);
        //Debug.Log("AI moving to platform: " + moveTo);


    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        OotsEvents.ClickedBot.Invoke(playerCore);
    }




    // TEMP Fog of War stuff - should be moved to fogofwarsystem

    RectTransform uiCanvasRectTransform;
    RectTransform injuryImageRectTransform;
    GameObject injuryImage;
    Transform overheadPlacer;
    Transform HUD;

    //GameManager GM;
    PlayerCore localPlayer;
    FogOfWarSystem FOW;

    void Update() {
        //if (GM == null) GM = FindObjectOfType<GameManager>();
        if (localPlayer == null) localPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCore>();
        if (FOW == null) FOW = FindObjectOfType<FogOfWarSystem>();

        if (HUD == null) HUD = GameObject.Find("HUD").GetComponent<Transform>();
        if (overheadPlacer == null) overheadPlacer = transform.FindChild("Overhead Menu Placer");
        if (injuryImage == null) {
            injuryImage = Instantiate(Resources.Load<GameObject>("Injury Image"));
            injuryImage.transform.SetParent(HUD, false);
            injuryImageRectTransform = injuryImage.GetComponent<RectTransform>();
        }
        if (uiCanvasRectTransform == null) uiCanvasRectTransform = GameObject.Find("UI Canvas").GetComponent<RectTransform>();

        if (playerCore.status.isInjured) {
            if (localPlayer.currentLocation.region != playerCore.currentLocation.region && FOW.gameObject.activeSelf) {
                injuryImage.SetActive(false);
            }
            else {
                float xScaler = uiCanvasRectTransform.rect.width / Screen.width;
                float yScaler = uiCanvasRectTransform.rect.height / Screen.height;
                Vector3 screenPointOfObjectCenter = Camera.main.WorldToScreenPoint(overheadPlacer.position); //localPlayer.transform.position
                injuryImageRectTransform.anchoredPosition = new Vector3(screenPointOfObjectCenter.x * xScaler, screenPointOfObjectCenter.y * yScaler, 0);
                injuryImage.SetActive(true);
            }
        }
        else {
            injuryImage.SetActive(false);
        }
    }


}
