﻿/*
* consider making this generic enough to handle context menu hosts as well as 3d item prefabs
*
*
*/

using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class OnClickTrigger : MonoBehaviour, IPointerClickHandler {

    [SerializeField]
    private string itemName;

    public void SetItemName(string name) {
        itemName = name;
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        //OotsEvents.ClickedSomething.Invoke(gameObject);

        OotsEvents.ClickedBagItem.Invoke(itemName);

        _DevUI.TextOut("clicked " + itemName);


    }


}
