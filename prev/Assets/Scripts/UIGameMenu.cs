﻿using UnityEngine;
using System.Collections;

public class UIGameMenu : MonoBehaviour {

    private UnityStandardAssets.ImageEffects.Blur blurEffect;

    void Awake() {
        blurEffect = Camera.main.GetComponent<UnityStandardAssets.ImageEffects.Blur>();
    }

    void OnEnable() {
        blurEffect.enabled = true;
    }

    void OnDisable() {
        if (blurEffect != null) blurEffect.enabled = false;
    }


    public void OnGotoMainMenuButtonPressed() {
        App.SwitchScene("Main Menu");
    }

    public void OnContinueButtonPressed() {
        gameObject.SetActive(false);
    }

}
