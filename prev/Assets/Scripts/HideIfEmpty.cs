﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HideIfEmpty : MonoBehaviour {
    Image image;

    void Awake() {
        image = GetComponent<Image>();
    }

    void Update() {
        if (transform.childCount == 0) {
            image.enabled = false;
        }
        else {
            image.enabled = true;
        }
    }
}
