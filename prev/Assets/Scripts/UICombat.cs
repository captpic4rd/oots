﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;


public class UICombat : MonoBehaviour {
    private BattleManager BM;
    //private RawImage leftCardImage, rightCardImage;
    private UnityStandardAssets.ImageEffects.Blur blurEffect;

    [SerializeField]
    private ParticleSystem hitLeftFX;
    [SerializeField]
    private ParticleSystem hitRightFX;

    Card leftCard;
    Card rightCard;

    // TODO: clean up text box stuff
    Text textBox;
    private List<string> textLines; // = new List<string>() { "", "", "", "" };


    GameObject injuredImageLeft;
    GameObject injuredImageRight;


    void Awake() {
        BM = FindObjectOfType<BattleManager>();
        blurEffect = Camera.main.GetComponent<UnityStandardAssets.ImageEffects.Blur>();

        leftCard = GameObject.Find("UI Card Left").GetComponent<Card>();
        rightCard = GameObject.Find("UI Card Right").GetComponent<Card>();


        var go = transform.FindChild("Combat Text Box");
        textBox = go.GetComponentInChildren<Text>();


        injuredImageLeft = GameObject.Find("Image (Injury) Left"); //transform.FindChild("Image (Injury) Left");
        injuredImageRight = GameObject.Find("Image (Injury) Right");
    }

    void OnEnable() {
        //10/19 blurEffect.enabled = true;

        textLines = new List<string>() { "", "", "", "", "", "", "", "" };

        leftCard.SetCard(BM.leftFighter.card);
        rightCard.SetCard(BM.rightFighter.card);

        OotsEvents.CombatHit.AddListener(OnCombatHit);
        OotsEvents.CombatMessage.AddListener(OnCombatMessage);
    }

    void OnDisable() {
        //10/19 if (blurEffect != null) blurEffect.enabled = false;

        OotsEvents.CombatHit.RemoveListener(OnCombatHit);
        OotsEvents.CombatMessage.RemoveListener(OnCombatMessage);
    }

    void Update() {
        leftCard.UpdateHealth(BM.leftFighter.currentHealth);
        rightCard.UpdateHealth(BM.rightFighter.currentHealth);

        textBox.text = string.Join("\n", textLines.ToArray());


        // Injury/combat status display
        if (BM.leftFighter.playerRef.status.isInjured)
            injuredImageLeft.SetActive(true);
        else
            injuredImageLeft.SetActive(false);

        if (BM.rightFighter.playerRef != null && BM.rightFighter.playerRef.status.isInjured)
            injuredImageRight.SetActive(true);
        else
            injuredImageRight.SetActive(false);

    }

    private void OnCombatHit(string whichSide) {
        if (whichSide == "left")
            hitLeftFX.Play();
        else if (whichSide == "right")
            hitRightFX.Play();
    }

    public void OnCombatAttackButtonPressed() {
        OotsEvents.ChoseCombatAction.Invoke("Attack");
    }

    public void OnCombatFleeButtonPressed() {
        OotsEvents.ChoseCombatAction.Invoke("Flee");
    }

    public void OnCombatMessage(string msg) {
        textLines.RemoveAt(0);
        textLines.Add(msg);
    }

}
