﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DiceManager : MonoBehaviour {

	public DiceController diceController;
	public GameObject cup;
	// Use this for initialization

	void Awake(){
		OotsEvents.ChoseAttack.AddListener (RollAttackDice);
		OotsEvents.EnteredCombat.AddListener (ShowDice);
		OotsEvents.AttemptedActionWhileInjured.AddListener (RollInjuryDie);
		OotsEvents.EndingTurnAtNightWithExposure.AddListener (RollExposureDice);
		OotsEvents.ChangeWeather.AddListener (RollWeatherDie);
	}

	void Start(){
		HideDice ();
	}

	void HideDice(){
		OotsEvents.LeftCombat.RemoveListener (HideDice);
		OotsEvents.EnteredCombat.AddListener (ShowDice);
		cup.SetActive (false);
		diceController.CleanupDice ();
	}

	void ShowDice(){
		OotsEvents.LeftCombat.AddListener (HideDice);
		OotsEvents.EnteredCombat.RemoveListener (ShowDice);
		cup.SetActive (true);
	}

	void RollAttackDice(int attackPower){
		bool attackDie = true;
		diceController.rollResultsReady += OnAttackRollComplete;
		diceController.BeginDiceRoll (attackDie, attackPower);
		ShowDice ();
	}

	public void OnAttackRollComplete(List<int> results){
		int damage = 0;
		for (int i = 0; i < results.Count; i++) {
			damage += results [i];
		}
		diceController.rollResultsReady -= OnAttackRollComplete;
		OotsEvents.GotAttackDiceResult.Invoke (damage);
		HideDice ();
	}

	void RollExposureDice(int numDice){
		bool attackDie = false;
		diceController.BeginDiceRoll (attackDie, numDice);
		diceController.rollResultsReady += OnExposureRollComplete;
		ShowDice ();
	}

	void OnExposureRollComplete(List<int> results){
		diceController.rollResultsReady -= OnExposureRollComplete;
		bool injured = false;
		for (int i = 0; i < results.Count; i++) {
			if (results [i] >= 4) {
				injured = true;
			}
		}
		OotsEvents.GotExposureDieResult.Invoke (injured);
		HideDice ();
	}

	void RollInjuryDie(){
		bool attackDie = false;
		int numDice = 1;
		diceController.rollResultsReady += OnInjuryRollComplete;
		diceController.BeginDiceRoll (attackDie, numDice);
		ShowDice ();
	}

	private void OnInjuryRollComplete(List<int> results){
		int result = results [0];
		diceController.rollResultsReady -= OnInjuryRollComplete;
		OotsEvents.GotInjuryDieResult.Invoke (result);
		HideDice ();
	}

	void RollWeatherDie(){
		bool attackDie = false;
		diceController.BeginDiceRoll (attackDie, 1);
		diceController.rollResultsReady += OnWeatherRollComplete;
		ShowDice ();
	}

	void OnWeatherRollComplete(List<int> results){
		diceController.rollResultsReady -= OnWeatherRollComplete;
		OotsEvents.GotWeatherRoll.Invoke (results[0]);
		HideDice ();
	}
}
