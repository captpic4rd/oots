﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;
using System;

[RequireComponent(typeof(LocationPlatform))]
public class Location : MonoBehaviour, IPointerClickHandler {

    private List<string> availableResources = new List<string>();

    [HideInInspector]
    public List<string> stash = new List<string>();

    public bool isShelter;    //public bool hasFireplace;

    public bool canHunt;
    public bool canGather;
    public bool canStash;

	public int ID;

    [SerializeField]
    private int herbCount;
    [SerializeField]
    private int berryCount;
    [SerializeField]
    private int woodCount;

    public int encounterZone;       // hunting and encounter tier / area
    public int region;

	private int fireTurnsRemaining = 0;

    public ParticleSystem firepit;
	public GameObject wagon;

    [HideInInspector]
    public LocationPlatform platform;


    void Awake() {
        if (canGather) {
            InitializeResources();
        }

        platform = GetComponent<LocationPlatform>();

        Transform t = transform.FindChild("FX_Fire");
		if (t != null) {
			firepit = t.gameObject.GetComponent<ParticleSystem> ();
		} 

		OotsEvents.EndedTurn.AddListener (OnEndedTurn);
		OotsEvents.FrostEvent.AddListener (OnFrostEvent);
		OotsEvents.WagonEvent.AddListener (OnWagonEvent);
    }

    public void ToggleFire() {
        if (firepit.isPlaying) {
            firepit.Stop();
        }
        else {
            firepit.Play();
        }
    }



	public void UseWood() { 
		if (fireTurnsRemaining <= 0) {
			ToggleFire ();
			fireTurnsRemaining += 2;
		} else {
			fireTurnsRemaining += 2;
		}
	} // add wood to fire

	private void OnEndedTurn(){
		if (fireTurnsRemaining > 0) {
			fireTurnsRemaining -= 1;
			if (fireTurnsRemaining == 0) {
				ToggleFire ();
			}
		}
	}

    public void Stow(string item) {
        stash.Add(item);
    }

    public void Take(string item) {
        stash.Remove(item);
    }




    public List<string> TakeAll() {
        var tmp = new List<string>(stash);
        stash.Clear();

        return tmp;
    }


    private void InitializeResources() {
        for (int i = 0; i < berryCount; i++) {
            availableResources.Add("Berry");
        }
        for (int i = 0; i < woodCount; i++) {
            availableResources.Add("Wood");
        }
        for (int i = 0; i < herbCount; i++) {
            availableResources.Add("Herb");
        }
    }

	public bool WagonPresent(){
		if (wagon.transform.position == transform.position) {
			Debug.Log ("found wagon!");
			wagon.SetActive (false);
			wagon.transform.position = Vector3.zero;
			return true;
		} else {
			return false;
		}
	}

    public string GatherResource() {
        if (availableResources.Count != 0) {
            string choice = availableResources[Random.Range(0, availableResources.Count)];
            availableResources.Remove(choice);
            return choice;
        }
        else {
            return null;
        }
    }

    void IPointerClickHandler.OnPointerClick(PointerEventData eventData) {
        OotsEvents.ClickedLocation.Invoke();
    }

	public bool HasFire(){
		if (fireTurnsRemaining > 0) {
			return true;
		} else {
			return false;
		}
	}

	private void OnFrostEvent(){
		herbCount = (int)herbCount / 2;
	}

	private void OnWagonEvent(int whichLocation){
		if(ID == whichLocation){
			wagon.transform.position = transform.position;
			wagon.SetActive(true);
		}
		//Debug.Log ("Location: wagon active: "+wagon.activeSelf);
	}
}
