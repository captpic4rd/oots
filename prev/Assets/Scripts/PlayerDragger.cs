﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class PlayerDragger : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler {
    private const float k_Drag = 10.0f;
    private const float k_AngularDrag = 5.0f;

    private Plane boardPlane = new Plane(Vector3.up, new Vector3(0f, 0f, 0f));
    private Plane overPlane = new Plane(Vector3.up, new Vector3(0f, 5.0f, 0f));

    private SpringJoint dragger;
    private GameObject[] snapPoints;
    private Camera mainCamera;
    private Rigidbody rigidBody;
    private PlayerCore playerCore;

    private Color oldColor;
    private float oldDrag;
    private float oldAngularDrag;

    private bool isDragging;

    private void Awake() {
        mainCamera = Camera.main;
        rigidBody = GetComponent<Rigidbody>();
        playerCore = GetComponent<PlayerCore>();
        CacheSnapPoints();

        oldColor = GetComponentInChildren<Renderer>().material.color;
        dragger = Instantiate(Resources.Load<SpringJoint>("Dragger"));
    }

    private void Start() {
        // Uncomment these two lines to visualize dragger for debugging
        //var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //cube.transform.SetParent(dragger.transform, false);
    }

    private void CacheSnapPoints() {
        snapPoints = GameObject.FindGameObjectsWithTag("SnapPoint");
        if (snapPoints.Length == 0) {
            Debug.Log(gameObject + ".CacheSnapPoints() >>> no snapPoints found");
        }
    }

    private void SnapToNearestPoint() {
        // Snap to the platform which is closest to the cursor position

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        float dist;
        Vector3 hitPoint = Vector3.zero;

        if (boardPlane.Raycast(ray, out dist)) {
            hitPoint = ray.GetPoint(dist);
        }
        else {
            hitPoint = transform.position;
        }

        Transform tMin = null;
        float minDist = Mathf.Infinity;
        foreach (GameObject t in snapPoints) {
            Vector3 diff = t.transform.position - hitPoint;

            if (diff.sqrMagnitude < minDist) {
                tMin = t.transform;
                minDist = diff.sqrMagnitude;
            }
        }
        playerCore.MoveToPlatform(tMin.GetComponent<Location>());
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData eventData) {
        if (!playerCore.isMyTurn)
            return;

        if (playerCore.isInCombat)
            return;

        isDragging = true;

        GetComponentInChildren<Renderer>().material.color = new Color(1.75f, 1.75f, 1.75f);

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        float dist;
        Vector3 hitPoint = Vector3.zero;
        if (overPlane.Raycast(ray, out dist)) {
            hitPoint = ray.GetPoint(dist);
        }
        else {
            Debug.Log("PlayerDragger: camera misconfiguration - overPlane not hit");
        }

        dragger.transform.position = hitPoint;
        dragger.connectedBody = rigidBody;

        oldDrag = dragger.connectedBody.drag;
        oldAngularDrag = dragger.connectedBody.angularDrag;
        dragger.connectedBody.drag = k_Drag;
        dragger.connectedBody.angularDrag = k_AngularDrag;

        rigidBody.isKinematic = false;
    }

    void IDragHandler.OnDrag(PointerEventData eventData) {
        if (!isDragging)
            return;

        Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
        float dist;
        overPlane.Raycast(ray, out dist);
        dragger.transform.position = ray.GetPoint(dist);
    }

    void IPointerUpHandler.OnPointerUp(PointerEventData eventData) {
        if (!isDragging)
            return;

        isDragging = false;

        GetComponentInChildren<Renderer>().material.color = oldColor;

        if (dragger.connectedBody) {
            dragger.connectedBody.drag = oldDrag;
            dragger.connectedBody.angularDrag = oldAngularDrag;
            dragger.connectedBody = null;
        }
        rigidBody.isKinematic = true;
        SnapToNearestPoint();
    }
}
