﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;


public class UIActionMenu : MonoBehaviour {
    //GameManager GM;
    PlayerCore localPlayer;

    //GameObject locationContextMenu;
    RectTransform uiCanvasRectTransform;
    RectTransform contextMenuRectTransform;
    Transform placer;
    RawImage blankClickCatcher;

    // prefabs
    private Button dynamicButtonPrefab;

    // ui elements
    GameObject pvpActionPanel;

    // buttons
    Transform gatherButton;
    Transform stashButton;
    Transform huntButton;
    Transform restButton;
    Transform studyButton;
    Transform fireButton;

    void Awake() {
        //GM = FindObjectOfType<GameManager>();
        localPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCore>();

        uiCanvasRectTransform = GameObject.Find("UI Canvas").GetComponent<RectTransform>();
        contextMenuRectTransform = GameObject.Find("Location Context Menu").GetComponent<RectTransform>();
        blankClickCatcher = GameObject.Find("UI Canvas").GetComponent<RawImage>();

        GameObject lcm = GameObject.Find("Location Context Menu");
        gatherButton = lcm.transform.FindChild("Gather Button");
        stashButton = lcm.transform.FindChild("Stash Button");
        huntButton = lcm.transform.FindChild("Hunt Button");
        restButton = lcm.transform.FindChild("Rest Button");
        studyButton = lcm.transform.FindChild("Study Button");
        fireButton = lcm.transform.FindChild("Fire Button");
    }

    void OnEnable() {
        OotsEvents.ClickedNothing.AddListener(OnBlankClick);
        blankClickCatcher.enabled = true;
        SetupActionMenu();
    }

    void OnDisable() {
        OotsEvents.ClickedNothing.RemoveListener(OnBlankClick);
        blankClickCatcher.enabled = false;
    }

    void Update() {
        float xScaler = uiCanvasRectTransform.rect.width / Screen.width;
        float yScaler = uiCanvasRectTransform.rect.height / Screen.height;

        placer = localPlayer.transform.FindChild("Overhead Menu Placer");
        Vector3 screenPointOfObjectCenter = Camera.main.WorldToScreenPoint(placer.position); //localPlayer.transform.position
        contextMenuRectTransform.anchoredPosition = new Vector3(screenPointOfObjectCenter.x * xScaler, screenPointOfObjectCenter.y * yScaler, 0);
    }

    private void SetupActionMenu() {
        if (localPlayer.currentLocation == null) return;

        var availableActions = localPlayer.GetAvailableActions();

        // Update main action menu
        gatherButton.gameObject.SetActive(availableActions.Contains("Gather") ? true : false);
        stashButton.gameObject.SetActive(availableActions.Contains("Stash") ? true : false);
        huntButton.gameObject.SetActive(availableActions.Contains("Hunt") ? true : false);
        restButton.gameObject.SetActive(availableActions.Contains("Rest") ? true : false);
        studyButton.gameObject.SetActive(availableActions.Contains("Study") ? true : false);
        fireButton.gameObject.SetActive(availableActions.Contains("Fire") ? true : false);
    }


    public void OnBlankClick() {
        gameObject.SetActive(false);
    }

    public void OnGatherButtonPressed() {
        BoardAction ba = new BoardAction();
        ba.actionType = ActionType.Gather;
        OotsEvents.ChoseGameAction.Invoke(ba);
    }

    public void OnHuntButtonPressed() {
        BoardAction ba = new BoardAction();
        ba.actionType = ActionType.Hunt;
        OotsEvents.ChoseGameAction.Invoke(ba);
    }

    public void OnStudyButtonPressed() {
        BoardAction ba = new BoardAction();
        ba.actionType = ActionType.Study;
        OotsEvents.ChoseGameAction.Invoke(ba);
    }

    public void OnRestButtonPressed() {
        BoardAction ba = new BoardAction();
        ba.actionType = ActionType.Rest;
        OotsEvents.ChoseGameAction.Invoke(ba);
    }

    public void OnStashButtonPressed() {
        Debug.Log("OnStashButtonPressed");
        OotsEvents.OpenedStash.Invoke(localPlayer.currentLocation);
    }


    /*
    public void OnActionButtonPressed(string text) {
        BoardAction ba = new BoardAction();

        if (text == "Study") {
            ba.actionType = ActionType.Study;
        }
        else if (text == "Gather") {
            ba.actionType = ActionType.Gather;
        }
        else if (text == "Hunt") {
            ba.actionType = ActionType.Hunt;
        }
        else if (text == "Rest") {
            ba.actionType = ActionType.Rest;
        }
        else if (text == "Stash") {
            OnStashButtonPressed();
        }
        else {
            Debug.Log("UIManager.OnActionButtonPressed: button/action not implemented");
            return;
        }
        OotsEvents.ChoseGameAction.Invoke(ba);
    }
    */

}
