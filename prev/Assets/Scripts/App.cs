﻿using UnityEngine;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;
using System.IO;


public class App : MonoBehaviour {

    public static App instance;


    // Playthrough setup (stuff chosen on main menu)
    public string p1Character { get; set; }

    //List<string> selectedCharacters = new List<string>();


    // static data
    public static Dictionary<string, CardData> cardData = new Dictionary<string, CardData>();

    public static Dictionary<string, ItemData> itemData = new Dictionary<string, ItemData>();

    //public static ItemData[] itemData2 = new ItemData[(int)ItemType.Count];



    public static void SwitchScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }

    public static void Quit() {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    public static CardData GetRandomMonsterCard(int tier) {
        List<string> eligibleTypes = new List<string>();

        foreach (CardData card in cardData.Values) {
            if (card.cardType == "monster" && card.encounterTier <= tier) {
                string cn = card.name;
                eligibleTypes.Add(cn);
            }
        }

        if (eligibleTypes.Count == 0) {
            return cardData["Rabbit"];
        }

        string monsterType = eligibleTypes[Random.Range(0, eligibleTypes.Count)];
        return cardData[monsterType];
    }


    void Awake() {
        instance = this;
        DontDestroyOnLoad(gameObject);
        LoadItems();
        LoadCards();
        SceneManager.LoadScene("Main Menu");
    }

    void OnDestroy() {
        if (instance != null) {
            instance = null;
        }
    }

    void Update() {
        if (Input.GetKey(KeyCode.Escape)) {
            Quit();
        }
    }


    //[SerializeField]
    //GameObject baseItemPrefab;
    [SerializeField]
    GameObject berryPrefab;
    [SerializeField]
    GameObject herbPrefab;
    [SerializeField]
    GameObject woodPrefab;
    [SerializeField]
    GameObject crystalPrefab;
    [SerializeField]
    GameObject meatPrefab;
	[SerializeField]
	GameObject cookedMeatPrefab;


    private void LoadItems() {
        itemData.Add("Berry", new ItemData("Berry", berryPrefab));
        itemData.Add("Crystal", new ItemData("Crystal", crystalPrefab));
        itemData.Add("Wood", new ItemData("Wood", woodPrefab));
        itemData.Add("Herb", new ItemData("Herb", herbPrefab));
        itemData.Add("Meat", new ItemData("Meat", meatPrefab));
		itemData.Add ("Cooked Meat", new ItemData ("Cooked Meat", cookedMeatPrefab));
    }


    private void LoadCards() {
        //var monsterDataPath = Application.persistentDataPath + "\\monsters";
        int loadedCount = 0;
        string texturePath = "Cards/Textures/";
        //Material backMaterial = Resources.Load<Material>(basePath + "Back");

        foreach (string fn in Directory.GetFiles(Application.persistentDataPath, "*.txt")) {
            string name = Path.GetFileNameWithoutExtension(fn);
            string contents = File.ReadAllText(fn);
            //Debug.Log("Loading card: " + name + " from file " + fn);

            CardData cd = JsonUtility.FromJson<CardData>(contents); // non-generic version allow missing members?
            cd.frontTexture = Resources.Load<Texture>(texturePath + name);
            //cd.frontMaterial = Resources.Load<Material>(basePath + name);
            //cd.backMaterial = backMaterial;
            cardData.Add(name, cd);

            loadedCount++;
        }

        Debug.Log("Loaded " + loadedCount + " cards from " + Application.persistentDataPath);
    }


}

