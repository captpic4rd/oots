﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;


public class _DevUI : MonoBehaviour {

    public static _DevUI instance;
    private Text textBox;
    private List<string> textLines = new List<string>() { "", "", "", "" };

    //private Camera mainCamera;

    Dropdown enemyDropdown;

    void Awake() {
        instance = this;
        var go = transform.FindChild("Text Box");
        textBox = go.GetComponentInChildren<Text>();

        //mainCamera = Camera.main;


        enemyDropdown = GameObject.Find("Enemy Dropdown").GetComponent<Dropdown>();

        List<string> monsterNames = new List<string>();

        foreach (CardData card in App.cardData.Values) {
            if (card.cardType == "monster") {
                monsterNames.Add(card.name);
            }
        }

        enemyDropdown.AddOptions(monsterNames);

    }

    void OnDestroy() {
        if (instance != null) {
            instance = null;
        }
    }

    void Update() {
        // TODO: no alloc here

        textBox.text = string.Join("\n", textLines.ToArray());
    }

    public static void TextOut(string text) {
        instance.textLines.RemoveAt(0);
        instance.textLines.Add(text);

    }


    public void OnHealButton() {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player != null) {
            PlayerStatus ps = player.GetComponent<PlayerStatus>();
            ps.FullHeal();
        }
    }

    public void OnTestFightButton() {
        BattleManager BM = FindObjectOfType<BattleManager>();
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (player == null) {
            Debug.LogError("Player not found");
            return;
        }
        PlayerCore pc = player.GetComponent<PlayerCore>();

        if (!pc.isMyTurn) return;

        string text = enemyDropdown.options[enemyDropdown.value].text;
        Debug.Log("DevUI: starting fight with: " + text);
        BM.StartBattleWithMonster(pc, App.cardData[text]);
        pc.EnterCombat("test fight");
    }

}
