﻿// Changes behavior of play command to always start from preload scene.
// Reverts to edited scene on exit play mode.

using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;


[InitializeOnLoad]
public static class LoadPreloadSceneOnPressingPlay {
    const string preloadScene = "Assets/Scenes/_preload.unity";
    const string oldSceneKey = "preloaderOldScene";
    const string didSceneSwapKey = "preloaderDidSceneSwap";

    static LoadPreloadSceneOnPressingPlay() {
        // Make sure callback is only registered once. See https://issuetracker.unity3d.com/issues/unityeditor-dot-initializeonload-calls-the-constructor-twice-when-the-editor-opens
        EditorApplication.playmodeStateChanged -= StateChange;
        EditorApplication.playmodeStateChanged += StateChange;
    }

    static void StateChange() {
        //Debug.Log("Preloader: isPlaying:" + EditorApplication.isPlaying + ", isPlayingOrWillChangePlaymode:" + EditorApplication.isPlayingOrWillChangePlaymode);

        // Note: playmodeStateChanged is fired twice on play (before/after entering play mode) and twice more on stop (before/after leaving play mode)
        // This hooks into the first and last events in order to perform the scene switches in editor mode 
        
        // Play command just sent, editor still in edit mode
        if (!EditorApplication.isPlaying && EditorApplication.isPlayingOrWillChangePlaymode) {
            EditorApplication.playmodeStateChanged -= StateChange;

            if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo()) { // TODO: cancel entering playmode if cancel is pressed
                Debug.Log("Preloader: User selected \"Cancel\" on \"Save Scene?\" dialogue: play will proceed, but without scene swap");
                EditorPrefs.SetBool(didSceneSwapKey, false);
                return;
            }
            Debug.Log("Preloader: Switching to preload scene"); // TODO: do only if _preload scene isn't already loaded
            EditorPrefs.SetString(oldSceneKey, EditorSceneManager.GetActiveScene().path);
            EditorPrefs.SetBool(didSceneSwapKey, true);
            EditorSceneManager.OpenScene(preloadScene); // EditorSceneManager.GetSceneAt(0)
        }
        // Play mode left, just got back to editor mode
        else if (!EditorApplication.isPlaying && !EditorApplication.isPlayingOrWillChangePlaymode) {
            var needSwap = EditorPrefs.GetBool(didSceneSwapKey, false);
            var oldScene = EditorPrefs.GetString(oldSceneKey, "");

            if (needSwap && oldScene != "") {
                Debug.Log("Preloader: Switching editor back to pre-play scene");
                EditorSceneManager.OpenScene(oldScene);
            }
        }
    }
}

