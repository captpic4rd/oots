﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Card : MonoBehaviour {

    //public Transform front;
    public RawImage frontImage;
    public Text titleText;
    public Text descriptionText;
    public Text flavorText;

    public Text healthText;
    public Text armorText;

    public Text SWText;
    public Text SEText;


    public void SetCard(CardData cardData) {
        frontImage.texture = cardData.frontTexture;

        titleText.text = cardData.name;
        descriptionText.text = cardData.description;
        flavorText.text = cardData.flavorText;

        healthText.text = cardData.health.ToString();
        armorText.text = cardData.armor.ToString();


    }

    public void UpdateHealth(int newHealth) {
        healthText.text = newHealth.ToString();
    }


}
