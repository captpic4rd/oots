﻿using UnityEngine;
using System.Collections;

public class PlayerHumanController : MonoBehaviour {
    PlayerCore playerCore;

    void Awake() {
        playerCore = GetComponent<PlayerCore>();
    }

    void OnEnable() {
        OotsEvents.ChoseGameAction.AddListener(OnChoseGameAction);
    }

    void OnDisable() {
        OotsEvents.ChoseGameAction.RemoveListener(OnChoseGameAction);
    }


    //private void OnAttemptMove(Location where) {    }


    private void OnChoseGameAction(BoardAction ba) {
        Debug.Log("PlayerHumanController.OnChoseGameAction: selected action " + ba.actionType.ToString());

        // TODO: consider an array[ActionType] = fxn instead of this big if block
        // this script might not be needed anymore anyways, could just direct-call playerCore actions from UI

        if (!playerCore.isMyTurn)
            return;

        if (ba.actionType == ActionType.Study)
            playerCore.AttemptStudy();

        else if (ba.actionType == ActionType.Gather)
            playerCore.AttemptGather();

        else if (ba.actionType == ActionType.Hunt)
            playerCore.AttemptHunt();

        else if (ba.actionType == ActionType.Rest)
            playerCore.AttemptRest();

        else if (ba.actionType == ActionType.Attack)
            playerCore.ActionAttack(ba.targetPlayer);

        else if (ba.actionType == ActionType.TakeItem)
            playerCore.ActionTakeAll();

        else if (ba.actionType == ActionType.StowItem)
            playerCore.ActionStowAll();

        else if (ba.actionType == ActionType.UseItem)
            playerCore.ActionUse(ba.item);

        else
            Debug.Log("PlayerHumanController.OnChoseGameAction: Action not implemented");

    }






}
