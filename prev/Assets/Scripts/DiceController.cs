﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DiceController : MonoBehaviour {

	public GameObject attackDie;
	public GameObject d6Die;
	public float maxForce_x;
	public float minForce_x;
	public float maxForce_y;
	public float minForce_y;
	public float maxForce_z;
	public float minForce_z;

	private int numDiceToRoll;
	private List<int> results = new List<int>();
	private List<DieCore> diceCoresAttack = new List<DieCore>();
	private List<DieCore> diceCoresD6 = new List<DieCore> ();
	public delegate void RollResultsReady(List<int> results);
	public RollResultsReady rollResultsReady;
	private Vector3 disabledDicePosition = new Vector3 (100, 0, 0);

	void Awake () {
		CreateDicePools ();
	}

	//creates all the dice when the game starts
	void CreateDicePools(){
		//create list of ten attack dice
		for(int i = 0; i < 10; i++){
			GameObject d = Instantiate(attackDie, transform.position, transform.rotation) as GameObject;
			d.SetActive (false);
			d.transform.position = disabledDicePosition;
			DieCore dc = d.GetComponent<DieCore>() as DieCore;
			dc.dieLanded += GetFace;
			dc.id = i;
			diceCoresAttack.Add(dc);
		}

		//create list of ten d6
		for (int i = 0; i < 10; i++) {
			GameObject d = Instantiate (d6Die, transform.position, transform.rotation) as GameObject;
			d.SetActive (false);
			d.transform.position = disabledDicePosition;
			DieCore dc = d.GetComponent<DieCore> () as DieCore;
			dc.dieLanded += GetFace;
			dc.id = i;
			diceCoresD6.Add(dc);
		}
	}

	//called when a die stops moving and gets its face
	void GetFace(DieCore d){
		results.Add (d.face);
		if (results.Count == numDiceToRoll) {
			OnRollComplete ();
		}
	}

	//called when all the die have stopped moving. sends results upwards.
	void OnRollComplete(){
		rollResultsReady (results);
		results = new List<int> ();
		print (results);
	}

	//disables all dice and moves them off the board
	//called in preparation of a roll.
	public void CleanupDice(){
		//move them back to their start positions
		for(int i = 0; i < 10; i++){
			diceCoresAttack [i].transform.position = disabledDicePosition;
			diceCoresAttack[i].DisableGravity();
			diceCoresAttack [i].gameObject.SetActive (false);
		}

		for (int i = 0; i < 10; i++) {
			diceCoresD6 [i].transform.position = disabledDicePosition;
			diceCoresD6 [i].DisableGravity ();
			diceCoresD6 [i].gameObject.SetActive (false);
		}
	}

	//rolls a type and number of dice
	//calls CleanupDice() beforehand
	public void BeginDiceRoll(bool attackType, int numDice){
		CleanupDice ();
		Vector3 spawnStart = new Vector3 (transform.position.x - (numDice * 2) / 2, transform.position.y, transform.position.z);
		numDiceToRoll = numDice;
		if (attackType) {
			for (int i = 0; i < numDice; i++) {
				diceCoresAttack [i].gameObject.SetActive (true);
				diceCoresAttack [i].transform.position = new Vector3 (spawnStart.x + i * 2, spawnStart.y, spawnStart.z);
				Vector3 randomForce = new Vector3 (Random.Range (minForce_x, maxForce_x), Random.Range (minForce_y, maxForce_y), Random.Range (minForce_z, maxForce_z));
				diceCoresAttack [i].AddForce (randomForce);
				diceCoresAttack [i].EnableGravity ();
			}
		} else {
			for (int i = 0; i < numDice; i++) {
				diceCoresD6 [i].gameObject.SetActive (true);
				diceCoresD6 [i].transform.position = new Vector3 (spawnStart.x + i * 2, spawnStart.y, spawnStart.z);
				Vector3 randomForce = new Vector3 (Random.Range (minForce_x, maxForce_x), Random.Range (minForce_y, maxForce_y), Random.Range (minForce_z, maxForce_z));
				diceCoresD6 [i].AddForce (randomForce);
				diceCoresD6 [i].EnableGravity ();
			}

		}
	}
}
