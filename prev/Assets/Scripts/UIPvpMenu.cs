﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;
//using UnityEngine.EventSystems;


public class UIPvpMenu : MonoBehaviour {
    PlayerCore localPlayer;

    RectTransform uiCanvasRectTransform;
    GameObject pvpContextMenu;
    RectTransform pvpContextMenuRectTransform;

    Transform placer;

    RawImage blankClickCatcher;

    // prefabs
    private Button dynamicButtonPrefab;

    // ui elements
    GameObject pvpActionPanel;

    // target player
    PlayerCore targetPlayer;


    void Awake() {
        localPlayer = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerCore>();

        uiCanvasRectTransform = GameObject.Find("UI Canvas").GetComponent<RectTransform>();
        pvpContextMenu = GameObject.Find("PVP Context Menu");
        pvpContextMenuRectTransform = pvpContextMenu.GetComponent<RectTransform>();

        blankClickCatcher = GameObject.Find("UI Canvas").GetComponent<RawImage>();

        dynamicButtonPrefab = Resources.Load<Button>("Action Button");

        pvpActionPanel = GameObject.Find("Action Menu Panel");
    }

    void OnEnable() {
        OotsEvents.ClickedNothing.AddListener(OnBlankClick);
        blankClickCatcher.enabled = true;
        SetupActionMenu();
    }

    void OnDisable() {
        OotsEvents.ClickedNothing.RemoveListener(OnBlankClick);
        blankClickCatcher.enabled = false;
    }

    void Update() {
        if (targetPlayer == null) return;

        // Place the PVP menu over the bot's head
        float xScaler = uiCanvasRectTransform.rect.width / Screen.width;
        float yScaler = uiCanvasRectTransform.rect.height / Screen.height;
        placer = targetPlayer.transform.FindChild("Overhead Menu Placer");
        Vector3 screenPointOfObjectCenter = Camera.main.WorldToScreenPoint(placer.position); //localPlayer.transform.position
        pvpContextMenuRectTransform.anchoredPosition = new Vector3(screenPointOfObjectCenter.x * xScaler, screenPointOfObjectCenter.y * yScaler, 0);
    }


    // Called by UIManager before enabling this UI, so the attack button knows which player to attack 
    public void SetTargetPlayer(PlayerCore bot) {
        targetPlayer = bot;
    }


    // This sets up the (now obsolete) menu on the left of the screen with options to attack each player on the same platform
    // All actions used to be chosen from this menu, they've been moved into context menus which come up when you click a player or location
    private void SetupActionMenu() {
        if (localPlayer.currentLocation == null) return;

        // Update old attack menu
        foreach (GameObject button in GameObject.FindGameObjectsWithTag("DynaButton")) {
            //button.GetComponent<Button>().onClick.RemoveAllListeners();
            Destroy(button);
        }

        var attackTargets = localPlayer.GetNearbyPlayers();

        foreach (GameObject player in attackTargets) {
            PlayerCore pc = player.GetComponent<PlayerCore>();
            string playerName = pc.characterName;
            //GameObject targetPlayer = player;
            Button btn = Instantiate(dynamicButtonPrefab);
            btn.transform.SetParent(pvpActionPanel.transform, false);
            btn.GetComponentInChildren<Text>().text = "Attack " + playerName;
            btn.onClick.AddListener(() => OnAttackButtonPressed(pc));
        }

        // Update PVP context menu
        Button attackButton = pvpContextMenu.transform.FindChild("Attack Button").GetComponent<Button>();
        attackButton.onClick.RemoveAllListeners();
        attackButton.onClick.AddListener(() => OnAttackButtonPressed(targetPlayer));

    }

    private void OnAttackButtonPressed(PlayerCore target) {
        //Debug.Log("Attacking " + target);

        BoardAction ba = new BoardAction();
        ba.actionType = ActionType.Attack;
        ba.targetPlayer = target;
        OotsEvents.ChoseGameAction.Invoke(ba);
    }


    // When a context menu is displayed but a click happens off of it - the context menu should close
    // This should probably be handled in UIManager like all UI element enabling/disabling
    public void OnBlankClick() {
        gameObject.SetActive(false);
    }

}
