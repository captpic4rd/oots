﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public enum BattleControlType { AIPlayer, HumanPlayer, Monster }
//public enum CombatAbility { Shy, DoubleAttack }


public class BattleSurrogate {
    public BattleControlType controlType;
    public PlayerCore playerRef = null;
    public CardData card;
    public int maximumHealth;
    public int currentHealth;
    public int armor;

    public BattleSurrogate(PlayerCore player) {
        playerRef = player;
        card = App.cardData[player.characterName];
        maximumHealth = card.health;
        currentHealth = player.status.currentHealth;
        armor = card.armor;

        if (player.isLocalPlayer) {
            controlType = BattleControlType.HumanPlayer;
        }
        else {
            controlType = BattleControlType.AIPlayer;
        }
    }

    public BattleSurrogate(CardData _card) {
        card = _card;
        maximumHealth = card.health;
        currentHealth = maximumHealth;
        armor = card.armor;
        controlType = BattleControlType.Monster;
    }

    public string GetMove() {
        if (controlType == BattleControlType.Monster) {
            // execute move based on monster personality - defined in CardData

            return Random.Range(0, 100) < 90 ? "attack" : "flee";
        }
        else if (controlType == BattleControlType.AIPlayer) {
            // execute move based on AI player personality

            if (currentHealth <= 2) {
                return Random.Range(0, 100) < 20 ? "attack" : "flee";
            }
            else {
                return Random.Range(0, 100) < 95 ? "attack" : "flee";
            }

        }
        else {
            // TODO: get move from player
            return "";
        }
    }


    public bool TakeDamage(int amount) {
        // TODO: move damage code from ApplyAttack to hre
        // this should update the playerRef if it exists
        return false;
    }
}


public class BattleManager : MonoBehaviour {
    public BattleSurrogate leftFighter, rightFighter;

    private int damageDone = -1;

    private string playerMove = "";
    private DiceController diceController;

    void Awake(){
        //diceController = FindObjectOfType<DiceController>();
    }

    void OnEnable() { // TODO: it would make sense to have the player's BattleSurrogate listen for these events instead
        OotsEvents.ChoseCombatAction.AddListener(OnChoseCombatAction);
		OotsEvents.GotAttackDiceResult.AddListener (OnDiceRollResultsRecieved);
        
    }

    void OnDisable() {
		print ("Battle Manager disabled");
        OotsEvents.ChoseCombatAction.RemoveListener(OnChoseCombatAction);
		OotsEvents.GotAttackDiceResult.RemoveListener (OnDiceRollResultsRecieved);
	}

    private void OnChoseCombatAction(string move) {
        playerMove = move;
    }



    public void StartBattleWithMonster(PlayerCore initiator, int encounterTier) {
        leftFighter = new BattleSurrogate(initiator);
        CardData monsterCard = App.GetRandomMonsterCard(encounterTier);
        rightFighter = new BattleSurrogate(monsterCard);

        _DevUI.TextOut("StartBattleWithMonster (tier):" + initiator.characterName + " encountered " + monsterCard.name);

        StartCoroutine("ResolveBattle");
    }

    public void StartBattleWithMonster(PlayerCore initiator, CardData card) {
        leftFighter = new BattleSurrogate(initiator);
        CardData monsterCard = card;
        rightFighter = new BattleSurrogate(monsterCard);

        _DevUI.TextOut("StartBattleWithMonster (card):" + initiator.characterName + " encountered " + monsterCard.name);

        StartCoroutine("ResolveBattle");
    }

    public void StartBattleWithPlayer(PlayerCore initiator, PlayerCore defender) {
        leftFighter = new BattleSurrogate(initiator);
        rightFighter = new BattleSurrogate(defender);

        _DevUI.TextOut("(Attack) Starting combat with " + defender.characterName);

        StartCoroutine("ResolveBattle");
    }

    IEnumerator ResolveBattle() {
        BattleSurrogate[] fighters = new BattleSurrogate[2];

        // Set up turn order (for now player is always first)
        fighters[0] = leftFighter;
        fighters[1] = rightFighter;

        Debug.Log("A battle has begun between " + leftFighter.card.name + " and " + rightFighter.card.name);

        string move;

        bool isBattleFinished = false;
        while (!isBattleFinished) {
            for (int i = 0; i < fighters.Length; i++) {
                BattleSurrogate activeFighter = fighters[i];
                BattleSurrogate passiveFighter = fighters[i == 1 ? 0 : 1];

                // Get move
                if (activeFighter.controlType == BattleControlType.HumanPlayer) {
                    // Get player move by waiting for OotsEvents.ChoseCombatAction
                    playerMove = "";
                    yield return new WaitWhile(() => playerMove == "");
                    move = playerMove;
                }
                else {
                    // Get monster or non-human player move from combat AI
                    move = activeFighter.GetMove();       // TODO: combat move struct
                }
                move = move.ToLower();
                Debug.Log(activeFighter.card.name + " [" + move + "]");

                // Process move
                if (move == "attack") {
					_DevUI.TextOut (activeFighter.card.name + " attacked " + passiveFighter.card.name);
                    // Do the attack
                    damageDone = -1;
					int attackPower = activeFighter.card.attackPower;

					if (activeFighter.controlType == BattleControlType.HumanPlayer) {
						OotsEvents.ChoseAttack.Invoke (attackPower);
						yield return new WaitWhile (() => damageDone < 0);
					} else {
						damageDone = Random.Range (0, activeFighter.card.attackPower);
					}


                    bool isTargetKilled = ApplyAttack(activeFighter, passiveFighter, damageDone);

                    // Play hit FX
                    if (i == 0) OotsEvents.CombatHit.Invoke("right");
                    else if (i == 1) OotsEvents.CombatHit.Invoke("left");

                    // Did something die?
                    if (isTargetKilled) {
                        Debug.Log(activeFighter.card.name + " defeated " + passiveFighter.card.name);
                        _DevUI.TextOut(activeFighter.card.name + " defeated " + passiveFighter.card.name);

                        // Give loot
                        if (activeFighter.controlType == BattleControlType.HumanPlayer || activeFighter.controlType == BattleControlType.AIPlayer) {
                            if (passiveFighter.card.lootCount > 0) {
                                activeFighter.playerRef.GiveItem(passiveFighter.card.lootItem, passiveFighter.card.lootCount);
                                Debug.Log("Handing out loot for " + passiveFighter.card.name + " " + passiveFighter.card.lootCount + " " + passiveFighter.card.lootItem);
                            }
                        }
                        // Kill player      TODO: move this to ApplyAttack?
                        if (passiveFighter.controlType != BattleControlType.Monster)
                            passiveFighter.playerRef.Die();

                        isBattleFinished = true;
                        break;
                    }
                }
                else if (move == "flee") {
                    Debug.Log(activeFighter.card.name + " ran away");
                    _DevUI.TextOut(activeFighter.card.name + " ran away");
                    isBattleFinished = true;
                    break;
                }
				//Add some delay into human battles
				if (activeFighter.controlType == BattleControlType.HumanPlayer || passiveFighter.controlType == BattleControlType.HumanPlayer) {
					yield return new WaitForSeconds(0.25f);
				}
            }
        }
        yield return new WaitForSeconds(0.5f);

        if (leftFighter.playerRef != null) leftFighter.playerRef.DepartCombat();
        if (rightFighter.playerRef != null) rightFighter.playerRef.DepartCombat();

        yield return null;
    }

	void OnDiceRollResultsRecieved(int result){
		damageDone = result;
    }

    bool ApplyAttack(BattleSurrogate attacker, BattleSurrogate target, int intResultFromDice) { // returns true if target died as a result of the attack

        // TODO: take into account attack power

        Debug.Log(attacker.card.name + " is attacking " + target.card.name);
        //10/19 var damageRoll = Random.Range(1, 7);
        int damageRoll = intResultFromDice;
        int damage = damageRoll - target.armor;
        if (damage < 0) damage = 0;

        OotsEvents.CombatMessage.Invoke(attacker.card.name + " hit " + target.card.name + " for " + damage.ToString());

        if (damage > 0) {
            target.currentHealth -= damage;
            if (target.currentHealth <= 0) target.currentHealth = 0;

            if (target.playerRef != null) {
                if (target.currentHealth == 0 && !target.playerRef.status.isInjured) {
                    // If uninjured and health reaches 0, set to 1 and add injured status
                    target.currentHealth = 1;
                    target.playerRef.status.SetInjured(true);
                    OotsEvents.CombatMessage.Invoke(target.card.name + " was injured by the attack!");
                }

                // Update player status to reflect loss of health from BattleSurrogate
                target.playerRef.status.ModifyHealth(target.currentHealth, true);
            }
        }

        return target.currentHealth <= 0 ? true : false;
    }
}
