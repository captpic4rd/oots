﻿using UnityEngine;
using System.Collections;

public class PlayerStatus : MonoBehaviour {

    PlayerCore playerCore;

    public int currentHealth = 6;
    public int maximumHealth = 6;


    // TODO: set max levels for status values
	public int maxHunger = 8;

    public int exposure = 0;
    public int hunger = 0;
    public int tiredness = 0;

    public bool isInjured = false;
    public bool isSick = false;


    void Awake() {
        playerCore = GetComponent<PlayerCore>();
		hunger = maxHunger;
    }

    public void ModifyHealth(int amount, bool absolute) {
        if (absolute) {
            //Debug.Log("ModifyHealth: setting to " + amount.ToString());
            currentHealth = amount;
        }
        else {
            currentHealth += amount;
        }
        if (currentHealth < 0) {
            currentHealth = 0;
            //Debug.Log("ModifyHealth: player death");
        }
        else if (currentHealth > maximumHealth) {
            currentHealth = maximumHealth;
        }

        if (playerCore.isLocalPlayer) OotsEvents.ModifiedStatus.Invoke();
    }

    public void SetExposure(int amount) {
        exposure = amount;
        if (playerCore.isLocalPlayer) OotsEvents.ModifiedStatus.Invoke();
    }

    public void SetTiredness(int amount) {
        tiredness = amount;
        if (playerCore.isLocalPlayer) OotsEvents.ModifiedStatus.Invoke();
    }

	public void ModifyExposure(int amount, bool absolute) {
		if (absolute) {
			exposure = amount;
		} else {
			exposure += amount;
			if (exposure < 0) {
				exposure = 0;
			}
		}
        if (playerCore.isLocalPlayer) OotsEvents.ModifiedStatus.Invoke();
    }

    public void ModifyHunger(int amount) {
        hunger += amount;
        if (hunger < 0) {
            hunger = 0;
        }

		if (hunger > maxHunger) {
			hunger = maxHunger;
		}
        if (playerCore.isLocalPlayer) OotsEvents.ModifiedStatus.Invoke();
    }

    public void SetInjured(bool injured) {
        isInjured = injured;
        if (playerCore.isLocalPlayer) OotsEvents.ModifiedStatus.Invoke();
    }

    public void FullHeal() {
        currentHealth = maximumHealth;

        exposure = 0;
        hunger = 0;
        tiredness = 0;

        isInjured = false;
        isSick = false;

        if (playerCore.isLocalPlayer) OotsEvents.ModifiedStatus.Invoke();
    }

}
