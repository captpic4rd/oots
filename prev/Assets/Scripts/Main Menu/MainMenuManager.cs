﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour {

    private string selectedCharacter = "random";


    public void menuSelectCharacter(string who) {
        selectedCharacter = who;
        GameObject v = GameObject.Find("Text");
        v.GetComponent<Text>().text = "Play as " + who;
    }

    public void menuStartGame() {
        if (selectedCharacter == "random") {
            int x = Random.Range(0, 3);
            if (x == 0) { selectedCharacter = "Val"; }
            else if (x == 1) { selectedCharacter = "Enyo"; }
            else if (x == 2) { selectedCharacter = "Reinar"; }
        }

        Debug.Log("Playing as " + selectedCharacter);
        App.instance.p1Character = selectedCharacter;
        App.SwitchScene("Game");
    }
}
