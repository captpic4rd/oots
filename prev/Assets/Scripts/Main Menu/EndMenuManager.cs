﻿using UnityEngine;


public class EndMenuManager : MonoBehaviour {


    public void GoToMainMenu() {
        App.SwitchScene("Main Menu");
    }

}
